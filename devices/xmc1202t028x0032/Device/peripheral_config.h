#ifndef PERIPHERAL_CONFIG_H
#define PERIPHERAL_CONFIG_H

#include <xmc_scu.h>
#include <xmc_gpio.h>
#include <xmc_uart.h>
#include <xmc_bccu.h>
#include <xmc_ccu4.h>
#include <xmc_flash.h>
#include <xmc1_scu.h>

#include "ucid_prng.h"
#include "E120.h"		

#define NUMBER_OF_CHANNELS 1
#define NUM_OF_BCCU_CHANNELS (NUMBER_OF_CHANNELS > 9 ? 9 : NUMBER_OF_CHANNELS)
#define NUM_OF_CCU_CHANNELS ((NUMBER_OF_CHANNELS - 9) > 0 ? (NUMBER_OF_CHANNELS - 9) : 0)	

#define EEPROM_ADDRESS                  (uint32_t *)		(0x10001000U + UC_FLASH*1024 - 256)	

#define TX_PIN				P2_11	
#define RX_PIN				P2_9	
#define DIR_PIN				P2_10	

#define CH1R_PIN			P0_4	
#define CH1G_PIN			P0_5	
#define CH1B_PIN			P0_6	
#define CH2R_PIN			P0_7	
#define CH2G_PIN			P0_8	
#define CH2B_PIN			P0_9	
#define CH3R_PIN			P0_10	
#define CH3G_PIN			P0_14	
#define CH3B_PIN			P0_15	

#define CH1R_PWM			BCCU0_CH0
#define CH1G_PWM			BCCU0_CH1
#define CH1B_PWM			BCCU0_CH2
#define CH2R_PWM			BCCU0_CH3
#define CH2G_PWM			BCCU0_CH4
#define CH2B_PWM			BCCU0_CH5
#define CH3R_PWM			BCCU0_CH6
#define CH3G_PWM			BCCU0_CH7
#define CH3B_PWM			BCCU0_CH8

#define CH0W_PIN			P1_0
#define CH1W_PIN			P1_1	
#define CH2W_PIN			P1_2	
#define CH3W_PIN			P1_3	

#define CCU4_PERIOD 4095U

#define BACKDOOR_PIN  	P0_13



typedef struct dev_param {
	uint32_t my_uid;
	uint16_t start_slot_num;
	uint8_t	 DE0_level;
	uint8_t	 DE1_level;
	uint8_t	 DE2_level;
	uint8_t	 device_label[32];
} dev_param_t;


void NC_pin_init(void);
void bccu_init(void);
void uart_init(void);
void ccu4_init(void);
void prng_init(void);
void tune_fade_time(void);
void ccu_pwm_init(void);
void dim_leds (uint8_t *channels);
void disable_debug(void);

void read_params(uint32_t *address, dev_param_t *parameters);
void write_params(uint32_t *address, dev_param_t *parameters);

#endif /*PERIPHERAL_CONFIG_H*/
