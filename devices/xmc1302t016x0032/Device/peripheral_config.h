#ifndef PERIPHERAL_CONFIG_H
#define PERIPHERAL_CONFIG_H

#include <xmc_scu.h>
#include <xmc_gpio.h>
#include <xmc_uart.h>
#include <xmc_bccu.h>
#include <xmc_ccu4.h>
#include <xmc_flash.h>
#include <xmc1_scu.h>

#include "ucid_prng.h"
#include "E120.h"		

#define NUMBER_OF_CHANNELS 1

#define EEPROM_ADDRESS                  (uint32_t *)		(0x10001000U + UC_FLASH*1024 - 256)	

#define TX_PIN				P2_11	
#define RX_PIN				P2_9	
#define DIR_PIN				P2_10	

#define CH1_PIN                         P0_5	
#define CH2_PIN                         P0_6	
#define CH3_PIN                         P0_7	
#define CH4_PIN                         P0_8	
#define CH5_PIN                         P0_9	
#define CH6_PIN                         P0_14	
#define CH7_PIN                         P0_15

#define CH1_PWM                         BCCU0_CH1
#define CH2_PWM                         BCCU0_CH2
#define CH3_PWM                         BCCU0_CH3
#define CH4_PWM                         BCCU0_CH4
#define CH5_PWM                         BCCU0_CH5
#define CH6_PWM                         BCCU0_CH7
#define CH7_PWM                         BCCU0_CH8


#define BACKDOOR_PIN  	P0_0



typedef struct dev_param {
	uint32_t my_uid;
	uint16_t start_slot_num;
	uint8_t	 DE0_level;
	uint8_t	 DE1_level;
	uint8_t	 DE2_level;
	uint8_t	 device_label[32];
} dev_param_t;


void NC_pin_init(void);
void bccu_init(void);
void uart_init(void);
void prng_init(void);
void tune_fade_time(void);
void ccu_pwm_init(void);
void dim_leds (uint8_t *channels);
void disable_debug(void);

void read_params(uint32_t *address, dev_param_t *parameters);
void write_params(uint32_t *address, dev_param_t *parameters);

#endif /*PERIPHERAL_CONFIG_H*/
