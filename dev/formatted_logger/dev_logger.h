//=============================================================================
// Logger module (based on SEGGER RTT and systick)
//=============================================================================

#ifndef DEV_LOGGER_H
#define DEV_LOGGER_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "SEGGER_RTT.h"

#ifndef LOGGER_SUBSYSTEM
#define LOGGER_SUBSYSTEM "unknown"
#endif

#define LOG_TIMESTAMP_ENABLED   0


#if LOG_TIMESTAMP_ENABLED

#else
#define LOG_CLEAR()             SEGGER_RTT_WriteString(0, RTT_CTRL_CLEAR);

#define LOG_ERR(msg, ...)       SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_RED "\r\nerror| %s | " msg, LOGGER_SUBSYSTEM, __VA_ARGS__)
#define LOG_WARN(msg, ...)      SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_YELLOW "\r\nwarning| %s | " msg, LOGGER_SUBSYSTEM, __VA_ARGS__)
#define LOG_INFO(msg, ...)      SEGGER_RTT_printf(0, RTT_CTRL_RESET "\r\ninfo| %s | " msg, LOGGER_SUBSYSTEM, __VA_ARGS__)
#define LOG_TEXT(msg, ...)      SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_CYAN "\r\n%s | ", msg)
#define LOG_DBG(msg, ...)       SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_GREEN "\r\ndebug| %s | " msg, LOGGER_SUBSYSTEM, __VA_ARGS__)
#endif


bool IsTimestampEnabled(void);
//void EnableTimestamp(void);
//char * GetCurTime(void);
void LoggerInit(void);

#endif // DEV_LOGGER_H