#include "peripheral_config.h"
#include "RDM_functions.h"

uint32_t fade_prescaler = 0x03;
uint16_t DE0_level = 100;
uint16_t DE1_level = 100;
uint16_t DE2_level = 100;
const uint8_t ccu_channels = NUMBER_OF_CHANNELS - 9;

const uint16_t NO_OF_BCCU_CHANNELS[9] = {0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F, 0x7F,
    0xFF, 0x1FF};

const XMC_GPIO_CONFIG_t NC_pin_config = {.mode =
                                             XMC_GPIO_MODE_INPUT_PULL_DOWN,
    .input_hysteresis = XMC_GPIO_INPUT_HYSTERESIS_STANDARD};

void NC_pin_init(void) {
  XMC_GPIO_Init(P2_0, &NC_pin_config);
  XMC_GPIO_Init(P2_6, &NC_pin_config);
  XMC_GPIO_Init(P2_7, &NC_pin_config);
  XMC_GPIO_Init(P2_8, &NC_pin_config);
  XMC_GPIO_Init(P0_9, &NC_pin_config);
  XMC_GPIO_SetMode(BACKDOOR_PIN, XMC_GPIO_MODE_INPUT_PULL_DOWN);  // BMI UPM
}

//  BCCU
const XMC_GPIO_CONFIG_t PWM_config = {
#ifdef BCR430
    .mode = XMC_GPIO_MODE_OUTPUT_OPEN_DRAIN_ALT1,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_HIGH
#elif BCR402
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT1,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW
#endif
};

const XMC_GPIO_CONFIG_t unused_pin_config = {
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW};

XMC_BCCU_GLOBAL_CONFIG_t bccu_global_config = {
    .fclk_ps = 0x10,                     // 4MHz @PCLK 64MHz (divider 16)
    .dclk_ps = 0x6D,                     // 300kHz
    .bclk_sel = XMC_BCCU_BCLK_MODE_FAST  // 4MHz bit stream
};

XMC_BCCU_DIM_CONFIG_t bccu_dim_config = {
    .dim_div = 0x0,
    .dither_en = 1,
    .cur_sel = XMC_BCCU_DIM_CURVE_FINE};

XMC_BCCU_CH_CONFIG_t pdm_global_config = {.pack_thresh = 3,
    .pack_en = true,
    .dim_sel = XMC_BCCU_CH_DIMMING_SOURCE_DE0,
    .flick_wd_en = XMC_BCCU_CH_FLICKER_WD_EN};

XMC_GPIO_CONFIG_t PWM_CCU40_config = {
    .mode = XMC_GPIO_MODE_OUTPUT_OPEN_DRAIN_ALT2,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_HIGH};

void bccu_init(void) {
  XMC_GPIO_CONFIG_t config;
  for (int i = 0; i < 7; i++) {
    config = (i < NUMBER_OF_CHANNELS) ? PWM_config : unused_pin_config;
    switch (i) {
      case 0:
        XMC_GPIO_Init(CH1_PIN, &config);
        break;
      case 1:
        XMC_GPIO_Init(CH2_PIN, &config);
        break;
      case 2:
        XMC_GPIO_Init(CH3_PIN, &config);
        break;
      case 3:
        XMC_GPIO_Init(CH4_PIN, &config);
        break;
      case 4:
        XMC_GPIO_Init(CH5_PIN, &config);
        break;
      case 5:
        XMC_GPIO_Init(CH6_PIN, &config);
        break;
      case 6:
        XMC_GPIO_Init(CH7_PIN, &config);
        break;
    }
  }
  XMC_BCCU_GlobalInit(BCCU0, &bccu_global_config);
  XMC_BCCU_ConcurrentSetOutputPassiveLevel(BCCU0,
      NO_OF_BCCU_CHANNELS[NUMBER_OF_CHANNELS - 1],
      XMC_BCCU_CH_ACTIVE_LEVEL_LOW);  // only after XMC_BCCU_GlobalInit
                                      // function!
  for (int i = 0; i < 9; i++) {
    switch (i) {
      case 0:
        XMC_BCCU_CH_Init(CH1_PWM, &pdm_global_config);
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH1_PWM, fade_prescaler);
        break;
      case 1:
        XMC_BCCU_CH_Init(CH2_PWM, &pdm_global_config);
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH2_PWM, fade_prescaler);
        break;
      case 2:
        XMC_BCCU_CH_Init(CH3_PWM, &pdm_global_config);
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH3_PWM, fade_prescaler);
        break;
      case 3:
        XMC_BCCU_CH_Init(CH4_PWM, &pdm_global_config);
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH4_PWM, fade_prescaler);
        break;
      case 4:
        XMC_BCCU_CH_Init(CH5_PWM, &pdm_global_config);
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH5_PWM, fade_prescaler);
        break;
      case 5:
        XMC_BCCU_CH_Init(CH6_PWM, &pdm_global_config);
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH6_PWM, fade_prescaler);
        break;
      case 6:
        XMC_BCCU_CH_Init(CH7_PWM, &pdm_global_config);
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH7_PWM, fade_prescaler);
        break;
    }
  }
  XMC_BCCU_DIM_Init(BCCU0_DE0, &bccu_dim_config);
  XMC_BCCU_ConcurrentEnableChannels(
      BCCU0, NO_OF_BCCU_CHANNELS[NUMBER_OF_CHANNELS - 1]);
  XMC_BCCU_EnableDimmingEngine(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE0);
  XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE0, 4095U);
  XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE0);

  uint8_t init_dim_val[NUMBER_OF_CHANNELS] = {0};
  dim_leds(init_dim_val);
}

void dim_leds(uint8_t* channels) {
  for (int i = 0; i < NUMBER_OF_CHANNELS; i++) {
    uint16_t channel_value = 0;
    channel_value = ((channels[i] << 4) + (channels[i] >> 4));

    switch (i) {
      case 0:
        XMC_BCCU_CH_SetTargetIntensity(CH1_PWM, channel_value);
        break;
      case 1:
        XMC_BCCU_CH_SetTargetIntensity(CH2_PWM, channel_value);
        break;
      case 2:
        XMC_BCCU_CH_SetTargetIntensity(CH3_PWM, channel_value);
        break;
      case 3:
        XMC_BCCU_CH_SetTargetIntensity(CH4_PWM, channel_value);
        break;
      case 4:
        XMC_BCCU_CH_SetTargetIntensity(CH5_PWM, channel_value);
        break;
      case 5:
        XMC_BCCU_CH_SetTargetIntensity(CH6_PWM, channel_value);
        break;
      case 6:
        XMC_BCCU_CH_SetTargetIntensity(CH7_PWM, channel_value);
        break;
    }
  }
  XMC_BCCU_ConcurrentStartLinearWalk(
      BCCU0, NO_OF_BCCU_CHANNELS[NUMBER_OF_CHANNELS - 1]);
}

void tune_fade_time(void) {
  uint32_t linear_walk_complete = 0;
  for (int i = 0; i < NUMBER_OF_CHANNELS; i++)
    linear_walk_complete |= XMC_BCCU_IsLinearWalkComplete(BCCU0, i);
  if (linear_walk_complete) {
    fade_prescaler--;
    XMC_BCCU_ConcurrentAbortLinearWalk(
        BCCU0, NO_OF_BCCU_CHANNELS[NUMBER_OF_CHANNELS - 1]);
  } else {
    fade_prescaler++;
  }
  for (int i = 0; i < NUMBER_OF_CHANNELS; i++) {
    switch (i) {
      case 0:
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH1_PWM, fade_prescaler);
        break;
      case 1:
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH2_PWM, fade_prescaler);
        break;
      case 2:
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH3_PWM, fade_prescaler);
        break;
      case 3:
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH4_PWM, fade_prescaler);
        break;
      case 4:
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH5_PWM, fade_prescaler);
        break;
      case 5:
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH6_PWM, fade_prescaler);
        break;
      case 6:
        XMC_BCCU_CH_SetLinearWalkPrescaler(CH7_PWM, fade_prescaler);
        break;
    }
  }
}

// UART
XMC_USIC_CH_t* DMX = XMC_UART0_CH1;

const XMC_UART_CH_CONFIG_t dmx_config = {.baudrate = 250000U,
    .data_bits = 8U,
    .frame_length = 8U,
    .oversampling = 16U,
    .stop_bits = 2,
    .parity_mode = XMC_USIC_CH_PARITY_MODE_NONE};

const XMC_GPIO_CONFIG_t DMX_dir_config = {.mode =
                                              XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW};

const XMC_GPIO_CONFIG_t DMX_rx_config = {.mode = XMC_GPIO_MODE_INPUT_TRISTATE,
    .input_hysteresis = XMC_GPIO_INPUT_HYSTERESIS_STANDARD};

const XMC_GPIO_CONFIG_t DMX_tx_config = {
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT7};

void uart_init(void) {
  XMC_GPIO_Init(DIR_PIN, &DMX_dir_config);
  XMC_GPIO_Init(TX_PIN, &DMX_tx_config);
  XMC_GPIO_Init(RX_PIN, &DMX_rx_config);

  XMC_GPIO_EnableDigitalInput(DIR_PIN);
  XMC_GPIO_EnableDigitalInput(TX_PIN);
  XMC_GPIO_EnableDigitalInput(RX_PIN);

  XMC_UART_CH_Init(DMX, &dmx_config);
  XMC_UART_CH_SetInputSource(DMX, XMC_UART_CH_INPUT_RXD, USIC0_C1_DX0_DX3INS);
  XMC_UART_CH_SetInputSource(DMX, XMC_UART_CH_INPUT_RXD1, USIC0_C1_DX3_P2_9);

  XMC_USIC_CH_RXFIFO_Configure(DMX, 0U, XMC_USIC_CH_FIFO_SIZE_2WORDS, 0U);

  XMC_USIC_CH_RXFIFO_SetInterruptNodePointer(
      DMX, XMC_USIC_CH_RXFIFO_INTERRUPT_NODE_POINTER_STANDARD, 0);

  XMC_UART_CH_EnableEvent(
      DMX, (uint32_t)XMC_UART_CH_EVENT_SYNCHRONIZATION_BREAK);
  XMC_USIC_CH_SetInterruptNodePointer(
      DMX, XMC_USIC_CH_INTERRUPT_NODE_POINTER_PROTOCOL, 0);
  XMC_USIC_CH_SetInterruptNodePointer(
      DMX, XMC_USIC_CH_INTERRUPT_NODE_POINTER_TRANSMIT_BUFFER, 0);
  NVIC_SetPriority(USIC0_0_IRQn, 3U);
  NVIC_EnableIRQ(USIC0_0_IRQn);

  XMC_UART_CH_Start(DMX);
}

void disable_debug(void) {
  uint32_t bmi_value;

  bmi_value = XMC_FLASH_ReadWord((uint32_t*)0x10000E00);
  if ((bmi_value & 0x0000ffffU) != 0xF8C1U) {
    XMC1000_BmiInstallationReq(0xF8C1);
  }
  if (XMC_GPIO_GetInput(BACKDOOR_PIN)) {
    XMC1000_BmiInstallationReq(0xF8C3);
  }
}

void prng_init(void) {
  uint32_t* UCIDptr;
  uint32_t UniqueChipID[4] = {0, 0, 0, 0};
  uint32_t Count;
  UCIDptr = (uint32_t*)0x10000FF0;
  uint16_t UCID[8];
  for (Count = 0; Count < 4; Count++) {
    UniqueChipID[Count] = *UCIDptr;
    UCID[2 * Count] = (UniqueChipID[Count] >> 16) & 0xFFFF;
    UCID[2 * Count + 1] = (UniqueChipID[Count] >> 0) & 0xFFFF;
    UCIDptr++;
  }
  XMC_PRNG_INIT_t prng_config = {.key_words[0] = UCID[0],
      .key_words[1] = UCID[1],
      .key_words[2] = UCID[2],
      .key_words[3] = UCID[3],
      .key_words[4] = UCID[4],
      .key_words[5] = UCID[5],
      .key_words[6] = UCID[6],
      .key_words[7] = UCID[7],
      .block_size = XMC_PRNG_RDBS_BYTE};

  XMC_PRNG_Init(&prng_config);
}

void read_params(uint32_t* address, dev_param_t* parameters) {
  uint8_t parameters_size =
      (sizeof(dev_param_t) / XMC_FLASH_BYTES_PER_BLOCK) + 1;
  XMC_FLASH_ReadBlocks(address, (uint32_t*)parameters, parameters_size);
  while (XMC_FLASH_GetStatus() != XMC_FLASH_STATUS_OK) {
  }
}

void write_params(uint32_t* address, dev_param_t* parameters) {
  uint8_t parameters_size =
      (sizeof(dev_param_t) / XMC_FLASH_BYTES_PER_BLOCK) + 1;

  __disable_irq();
  XMC_FLASH_ErasePage(address);
  while (XMC_FLASH_GetStatus() != XMC_FLASH_STATUS_OK) {
  }
  XMC_FLASH_WriteBlocks(
      address, (uint32_t*)parameters, parameters_size, true);
  while (XMC_FLASH_GetStatus() != XMC_FLASH_STATUS_OK) {
  }
  __enable_irq();
}

// EOF