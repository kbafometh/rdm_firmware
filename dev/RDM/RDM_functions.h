#ifndef RDM_FUNCTIONS_H
#define RDM_FUNCTIONS_H

#include "peripheral_config.h"

#define DEVICE_MODEL_ID1 0x00 /* MSB */
#define DEVICE_MODEL_ID0 0x01 /* LSB */

#define SW_VERSION_ID3 0x03 /* MSB */
#define SW_VERSION_ID2 0xC6
#define SW_VERSION_ID1 0x1F
#define SW_VERSION_ID0 0xB3 /* LSB */
#define FIRMWARE_VERSION "V0.9.0.A"

static uint64_t get_my_UID(void);
static uint64_t get_random_UID(void);
static void delay_us(uint16_t us_count);
static uint16_t calc_crc(uint8_t * data, uint8_t length);
static bool check_broadcast(uint8_t * rdm_msg);
static bool check_own_uid(uint8_t * rdm_msg);
static void TransmitDmxSlotValues(uint8_t * pBuf, uint8_t nrOfSlots, bool BREAK);
static void send_disc_response(uint8_t * rdm_msg);
static void send_rdm_msg(uint8_t * rdm_msg, uint8_t command_class, uint8_t data_len);
static void send_get_device_info_response(uint8_t * rdm_msg);
static void send_get_supported_parameters_response(uint8_t * rdm_msg);
static void send_get_device_label_response(uint8_t * rdm_msg);
static void send_get_software_version_response(uint8_t * rdm_msg);
static void send_disc_mute_response(uint8_t * rdm_msg);
static void send_disc_un_mute_response(uint8_t * rdm_msg);
static void send_get_dmx_start_address_response(uint8_t * rdm_msg);
static void send_get_device_model_description_response(uint8_t * rdm_msg);
static void send_get_manufacturer_label_response(uint8_t * rdm_msg);
static void send_get_identify_device_response(uint8_t * rdm_msg, uint8_t state);
static void update_device_parameters(void);
static void send_set_dmx_start_address_response(uint8_t * rdm_msg);
static void send_set_identify_device_response(uint8_t * rdm_msg);
static void send_set_device_label_response(uint8_t * rdm_msg);
static void set_device_label(uint8_t * rdm_msg);
static void handle_rdm_msg(uint8_t * rdm_msg);
void handle_dmx_msg(void);

void rdm_init(void);
void work_mode(void);
static void send_get_parameter_description_response(uint8_t * rdm_msg);
static void send_get_DE0_level_response(uint8_t * rdm_msg);
static void send_set_DE0_level_response(uint8_t * rdm_msg);

#endif /*RDM_FUNCTIONS_H*/