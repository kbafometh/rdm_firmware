/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*            (c) 2014 - 2018 SEGGER Microcontroller GmbH             *
*                                                                    *
*           www.segger.com     Support: support@segger.com           *
*                                                                    *
**********************************************************************
*                                                                    *
* All rights reserved.                                               *
*                                                                    *
* Redistribution and use in source and binary forms, with or         *
* without modification, are permitted provided that the following    *
* conditions are met:                                                *
*                                                                    *
* - Redistributions of source code must retain the above copyright   *
*   notice, this list of conditions and the following disclaimer.    *
*                                                                    *
* - Neither the name of SEGGER Microcontroller GmbH                  *
*   nor the names of its contributors may be used to endorse or      *
*   promote products derived from this software without specific     *
*   prior written permission.                                        *
*                                                                    *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND             *
* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,        *
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF           *
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
* DISCLAIMED.                                                        *
* IN NO EVENT SHALL SEGGER Microcontroller GmbH BE LIABLE FOR        *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  *
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;    *
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF      *
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          *
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  *
* USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH   *
* DAMAGE.                                                            *
*                                                                    *
*********************************************************************/

#ifndef __mcu_h
#define __mcu_h

#if defined(XMC1100_Q024x0008) || defined(XMC1100_T016x0008) || defined(XMC1100_Q024x0016) || defined(XMC1100_Q040x0016) || defined(XMC1100_T016x0016) || defined(XMC1100_T038x0016) || defined(XMC1100_Q024x0032) || defined(XMC1100_Q040x0032) || defined(XMC1100_T016x0032) || defined(XMC1100_T038x0032) || defined(XMC1100_Q024x0064) || defined(XMC1100_Q040x0064) || defined(XMC1100_T016x0064) || defined(XMC1100_T038x0064)

#include "XMC1100.h"

#elif defined(XMC1201_Q040x0016) || defined(XMC1201_T028x0016) || defined(XMC1201_T028x0032) || defined(XMC1201_T038x0016) || defined(XMC1201_Q040x0032) || defined(XMC1201_T038x0032) || defined(XMC1201_Q040x0064) || defined(XMC1201_T038x0064) || defined(XMC1201_Q040x0128) || defined(XMC1201_T038x0128) || defined(XMC1200_T038x0200) || defined(XMC1201_Q040x0200) || defined(XMC1201_T038x0200) || defined(XMC1202_Q024x0016) || defined(XMC1202_Q040x0016) || defined(XMC1202_T016x0016) || defined(XMC1202_T028x0016) || defined(XMC1202_Q024x0032) || defined(XMC1202_Q040x0032) || defined(XMC1202_T016x0032) || defined(XMC1202_T028x0032) || defined(XMC1202_T028x0064) || defined(XMC1202_T016x0064)

#include "XMC1200.h"

#elif defined(XMC1301_Q024x0008) || defined(XMC1301_Q040x0008) || defined(XMC1301_T016x0008) || defined(XMC1301_T038x0008) || defined(XMC1301_Q024x0016) || defined(XMC1301_Q040x0016) || defined(XMC1301_T016x0016) || defined(XMC1301_T038x0016) || defined(XMC1301_Q040x0032) || defined(XMC1301_T038x0032) || defined(XMC1301_T016x0032) || defined(XMC1302_T028x0016) || defined(XMC1302_T028x0032) || defined(XMC1302_T028x0064) || defined(XMC1302_T028x0128) || defined(XMC1302_T028x0200) || defined(XMC1302_Q024x0016) || defined(XMC1302_Q040x0016) || defined(XMC1302_T016x0016) || defined(XMC1302_T038x0016) || defined(XMC1302_Q024x0032) || defined(XMC1302_Q040x0032) || defined(XMC1302_T016x0032) || defined(XMC1302_T038x0032) || defined(XMC1302_Q024x0064) || defined(XMC1302_Q040x0064) || defined(XMC1301_T038x0064) || defined(XMC1302_T038x0064) || defined(XMC1302_Q040x0128) || defined(XMC1302_T038x0128) || defined(XMC1302_T038x0200) || defined(XMC1302_Q040x0200)

#include "XMC1300.h"

#elif defined(XMC1401_Q048x0064) || defined(XMC1401_Q048x0128) || defined(XMC1401_F064x0064) || defined(XMC1401_F064x0128) || defined(XMC1402_T038x0032) || defined(XMC1402_T038x0064) || defined(XMC1402_T038x0128) || defined(XMC1402_T038x0200) || defined(XMC1402_Q040x0032) || defined(XMC1402-Q040x0064) || defined(XMC1402_Q040x0128) || defined(XMC1402_Q040x0200) || defined(XMC1402_Q048x0032) || defined(XMC1402_Q048x0064) || defined(XMC1402_Q048x0128) || defined(XMC1402_Q048x0200) || defined(XMC1402_Q064x0064) || defined(XMC1402_Q064x0128) || defined(XMC1402_Q064x0200) || defined(XMC1402_F064x0064) || defined(XMC1402_F064x0128) || defined(XMC1402_F064x0200) || defined(XMC1403_Q040x0064) || defined(XMC1403_Q040x0128) || defined(XMC1403_Q040x0200) || defined(XMC1403_Q048x0064) || defined(XMC1403_Q048x0128) || defined(XMC1403_Q048x0200) || defined(XMC1403_Q064x0064) || defined(XMC1403_Q064x0128) || defined(XMC1403_Q064x0200) || defined(XMC1404_Q048x0064) || defined(XMC1404_Q048x0128) || defined(XMC1404_Q048x0200) || defined(XMC1404_Q064x0064) || defined(XMC1404_Q064x0128) || defined(XMC1404_Q064x0200) || defined(XMC1404_F064x0064) || defined(XMC1404_F064x0128) || defined(XMC1404_F064x0200)

#include "XMC1400.h"

#endif

#endif
