#include "RDM_functions.h"

uint8_t MCU_temperature;

uint64_t my_UID;

extern XMC_USIC_CH_t * DMX;
uint8_t channels[NUMBER_OF_CHANNELS] = {0};

extern uint32_t tick;
uint32_t tickstart = 0;

uint16_t current_slot_num;

uint8_t received_slots[513];

uint8_t rdm_response_msg[128];

uint8_t rdm_message_count = 0;

char device_label[32];

uint8_t intensity = 0;
const uint16_t MAX_DMX_SLOTS = 513U;

static bool dim_flag = false;
static bool new_frame = false;
static bool rdm_muted = false;
static uint8_t identify_state;
dev_param_t current_params;

uint16_t power_percentage[101] = {0U, 41U, 81U, 122U, 162U, 203U, 243U, 284U, 324U, 365U, 405U,
    446U, 486U, 527U, 567U, 608U, 648U, 689U, 729U, 770U, 810U, 851U, 891U, 932U, 972U, 1013U,
    1053U, 1094U, 1134U, 1175U, 1215U, 1256U, 1296U, 1337U, 1377U, 1418U, 1458U, 1499U, 1539U,
    1580U, 1620U, 1661U, 1701U, 1742U, 1782U, 1823U, 1863U, 1904U, 1944U, 1985U, 2025U, 2066U,
    2106U, 2147U, 2187U, 2228U, 2268U, 2309U, 2349U, 2390U, 2430U, 2431U, 2471U, 2512U, 2552U,
    2593U, 2633U, 2674U, 2714U, 2755U, 2795U, 2836U, 2876U, 2917U, 2957U, 2998U, 3008U, 3049U,
    3089U, 3130U, 3170U, 3211U, 3251U, 3292U, 3332U, 3373U, 3413U, 3454U, 3494U, 3535U, 3575U,
    3616U, 3656U, 3697U, 3737U, 3778U, 3818U, 3859U, 3899U, 3940U, 4095U};

///////////////////////////////////////////////////////////////////////////////
void rdm_init(void)
{
#if defined(UID_JFLASH)
    my_UID = get_my_UID();
#elif defined(UID_RANDOM)
    my_UID = get_random_UID();
#else
#error "UID selection method undefined"
#endif

    bool error_values = false;

    read_params(EEPROM_ADDRESS, &current_params);

    if ((current_params.start_slot_num + NUMBER_OF_CHANNELS > 512U) ||
        current_params.start_slot_num == 0)
    {
        error_values = true;
    }

    if (current_params.DE0_level > 100 || current_params.DE1_level > 100 ||
        current_params.DE2_level > 100)
    {
        error_values = true;
    }

    if (current_params.my_uid != (my_UID & UINT32_MAX))
    {
        error_values = true;
    }

    if (error_values)
    {
        dev_param_t standart;
        standart.DE0_level = 100;
        standart.DE1_level = 100;
        standart.DE2_level = 100;
        standart.my_uid = my_UID;
        standart.start_slot_num = 1;
        standart.device_label[0] = 'w';
        standart.device_label[1] = 'r';
        standart.device_label[2] = 'i';
        standart.device_label[3] = 't';
        standart.device_label[4] = 'e';
        standart.device_label[5] = ' ';
        standart.device_label[6] = 'y';
        standart.device_label[7] = 'o';
        standart.device_label[8] = 'u';
        standart.device_label[9] = 'r';
        standart.device_label[10] = ' ';
        standart.device_label[11] = 'l';
        standart.device_label[12] = 'a';
        standart.device_label[13] = 'b';
        standart.device_label[14] = 'e';
        standart.device_label[15] = 'l';
        for (uint8_t i = 16; i < 32; i++)
        {
            standart.device_label[i] = 0;
        }
        write_params(EEPROM_ADDRESS, &standart);
        NVIC_SystemReset();
    }


    read_params(EEPROM_ADDRESS, &current_params);

    XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE0, power_percentage[current_params.DE0_level]);
    XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE0);
    XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE1, power_percentage[current_params.DE1_level]);
    XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE1);
    XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE2, power_percentage[current_params.DE2_level]);
    XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE2);
}

///////////////////////////////////////////////////////////////////////////////
void work_mode(void)
{
    while (1)
    {
        MCU_temperature = XMC_SCU_CalcTemperature();
        if (new_frame)
        {
            handle_rdm_msg(received_slots);
            uint8_t len = received_slots[2] + 2;
            memset(received_slots, NULL, len);
            new_frame = false;
        }
        if (dim_flag)
        {
            dim_leds(channels);
            dim_flag = false;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
void handle_dmx_msg(void)
{
    uint16_t read_byte = 0U;

    if ((XMC_USIC_CH_RXFIFO_GetEvent(DMX) & USIC_CH_TRBSR_SRBI_Msk) == USIC_CH_TRBSR_SRBI_Msk)
    {
        read_byte = XMC_USIC_CH_RXFIFO_GetData(DMX);
        XMC_USIC_CH_RXFIFO_ClearEvent(DMX, (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_STANDARD);
        current_slot_num++;
    }

    if ((XMC_UART_CH_GetStatusFlag(DMX) & USIC_CH_PSR_ASCMode_SBD_Msk) ==
        USIC_CH_PSR_ASCMode_SBD_Msk)
    {
        tune_fade_time();
        XMC_UART_CH_ClearStatusFlag(
            DMX, (uint32_t)XMC_UART_CH_STATUS_FLAG_SYNCHRONIZATION_BREAK_DETECTED);
        current_slot_num = 0U;
        while (false == XMC_USIC_CH_RXFIFO_IsEmpty(DMX))
        {
            *received_slots = (uint8_t)XMC_USIC_CH_RXFIFO_GetData(DMX);
        }
        /* Enable USIC Standard Receive Buffer interrupt */
        XMC_USIC_CH_RXFIFO_EnableEvent(DMX, (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_CONF_STANDARD);
    }

    if ((current_slot_num - 1U) < MAX_DMX_SLOTS)
    {
        *((received_slots) + ((current_slot_num - 1U))) = (uint8_t)(read_byte & 0xFFU);
        if ((current_slot_num) == MAX_DMX_SLOTS)
        {
            /* Disable USIC Standard Receive Buffer interrupt */
            XMC_USIC_CH_RXFIFO_DisableEvent(DMX, (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_CONF_STANDARD);
            /* Clear receive buffer status */
            XMC_USIC_CH_RXFIFO_ClearEvent(
                DMX, (uint32_t)((uint32_t)XMC_USIC_CH_RXFIFO_EVENT_STANDARD |
                                (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_ERROR |
                                (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_ALTERNATE));
            XMC_USIC_CH_RXFIFO_Flush(DMX);
        }
    }

    if (received_slots[0] == SC_NULL)
    {
        if ((current_slot_num - 1) == current_params.start_slot_num + NUMBER_OF_CHANNELS)
        {
            /* Disable USIC Standard Receive Buffer interrupt */
            XMC_USIC_CH_RXFIFO_DisableEvent(DMX, (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_CONF_STANDARD);
            /* Clear receive buffer status */
            XMC_USIC_CH_RXFIFO_ClearEvent(
                DMX, (uint32_t)((uint32_t)XMC_USIC_CH_RXFIFO_EVENT_STANDARD |
                                (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_ERROR |
                                (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_ALTERNATE));
            XMC_USIC_CH_RXFIFO_Flush(DMX);
            /* Slot data reception is successful, calling user Callback function */
        }

        for (int i = 0; i < NUMBER_OF_CHANNELS; i++)
            channels[i] = received_slots[current_params.start_slot_num + i];
        dim_flag = true;
    }

    else if (received_slots[0] == E120_SC_RDM)
    {
        if (((current_slot_num - 2) == received_slots[2]) & (received_slots[2] != 0))
        {
            /* Disable USIC Standard Receive Buffer interrupt */
            XMC_USIC_CH_RXFIFO_DisableEvent(DMX, (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_CONF_STANDARD);
            /* Clear receive buffer status */
            XMC_USIC_CH_RXFIFO_ClearEvent(
                DMX, (uint32_t)((uint32_t)XMC_USIC_CH_RXFIFO_EVENT_STANDARD |
                                (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_ERROR |
                                (uint32_t)XMC_USIC_CH_RXFIFO_EVENT_ALTERNATE));
            XMC_USIC_CH_RXFIFO_Flush(DMX);
            XMC_UART_CH_ClearStatusFlag(DMX, XMC_UART_CH_STATUS_FLAG_RECEIVE_FRAME_FINISHED);
            new_frame = true;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
static void update_device_parameters(void)
{
    dev_param_t stored_params;
    bool update_flag = false;

    read_params(EEPROM_ADDRESS, &stored_params);

    if (stored_params.DE0_level != current_params.DE0_level ||
        stored_params.DE1_level != current_params.DE1_level ||
        stored_params.DE2_level != current_params.DE2_level ||
        stored_params.start_slot_num != current_params.start_slot_num ||
        stored_params.my_uid != current_params.my_uid)
    {
        update_flag = true;
    }

    for (uint8_t i = 0; i < 32; i++)
    {
        if (stored_params.device_label[i] != current_params.device_label[i])
        {
            update_flag = true;
        }
    }

    if (update_flag)
    {
        write_params(EEPROM_ADDRESS, &current_params);
    }
}

///////////////////////////////////////////////////////////////////////////////
static uint64_t get_my_UID(void)
{
    dev_param_t flash;
    uint64_t uid;

    uid = MAN_ID_H;
    uid <<= 8;
    uid |= MAN_ID_L;
    uid <<= 32;
    uid |= flash.my_uid;

    return uid;
}

///////////////////////////////////////////////////////////////////////////////
static uint64_t get_random_UID(void)
{
    uint64_t rand_uid;
    uint8_t rand;
    rand_uid = MAN_ID_H;
    rand_uid <<= 8;
    rand_uid |= MAN_ID_L;
    rand_uid <<= 8;
    rand = XMC_PRNG_GetPseudoRandomNumber();
    rand_uid |= rand;
    rand_uid <<= 8;
    rand = XMC_PRNG_GetPseudoRandomNumber();
    rand_uid |= rand;
    rand_uid <<= 8;
    rand = XMC_PRNG_GetPseudoRandomNumber();
    rand_uid |= rand;
    rand_uid <<= 8;
    rand = XMC_PRNG_GetPseudoRandomNumber();
    rand_uid |= rand;

    return rand_uid;
}

///////////////////////////////////////////////////////////////////////////////
static void delay_us(uint16_t us_count)
{
    tickstart = tick;
    while ((tick - tickstart) < us_count)
    {
        __NOP();
    }
}

///////////////////////////////////////////////////////////////////////////////
static uint16_t calc_crc(uint8_t * data, uint8_t length)
{
    uint16_t crc = 0;
    uint16_t i;

    for (i = 0; i < length; i++)
    {
        crc += *data++;
    }
    return crc;
}

///////////////////////////////////////////////////////////////////////////////
static bool check_broadcast(uint8_t * rdm_msg)
{
    if ((rdm_msg[5] == 0xFF) && (rdm_msg[6] == 0xFF) && (rdm_msg[7] == 0xFF) &&
        (rdm_msg[8] == 0xFF))
    {
        if (((rdm_msg[3] == 0xFF) && (rdm_msg[4] == 0xFF)) ||
            ((rdm_msg[3] == MAN_ID_H) && (rdm_msg[4] == MAN_ID_L)))
        {
            return true;
        }
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
static bool check_own_uid(uint8_t * rdm_msg)
{
    if ((rdm_msg[3] == ((my_UID >> 40) & 0xFF)) && (rdm_msg[4] == ((my_UID >> 32) & 0xFF)) &&
        (rdm_msg[5] == ((my_UID >> 24) & 0xFF)) && (rdm_msg[6] == ((my_UID >> 16) & 0xFF)) &&
        (rdm_msg[7] == ((my_UID >> 8) & 0xFF)) && (rdm_msg[8] == ((my_UID >> 0) & 0xFF)))
    {
        return true;
    }
    else
        return false;
}

///////////////////////////////////////////////////////////////////////////////
static void TransmitDmxSlotValues(uint8_t * pBuf, uint8_t nrOfSlots, bool BREAK)
{
    if (nrOfSlots > 1)
    {
        XMC_GPIO_SetOutputHigh(DIR_PIN);
        delay_us(10);

        if (BREAK == true)
        {
            XMC_UART_CH_SetBaudrate(DMX, 50000, 16U);
            XMC_UART_CH_Transmit(DMX, 0x00);
            XMC_UART_CH_SetBaudrate(DMX, 250000, 16U);
        }

        for (uint8_t k = 0; k < nrOfSlots; k++)
        {
            XMC_UART_CH_Transmit(DMX, pBuf[k]);
        }
    }

    delay_us(10);

    XMC_GPIO_SetOutputLow(DIR_PIN);
}

///////////////////////////////////////////////////////////////////////////////
static void send_disc_response(uint8_t * rdm_msg)
{
    delay_us(100);
    rdm_response_msg[0] = 0xFE;
    rdm_response_msg[1] = 0xFE;
    rdm_response_msg[2] = 0xFE;
    rdm_response_msg[3] = 0xFE;
    rdm_response_msg[4] = 0xFE;
    rdm_response_msg[5] = 0xFE;
    rdm_response_msg[6] = 0xFE;
    rdm_response_msg[7] = 0xAA;
    rdm_response_msg[8] = MAN_ID_H | 0xAA;
    rdm_response_msg[9] = MAN_ID_H | 0x55;
    rdm_response_msg[10] = MAN_ID_L | 0xAA;
    rdm_response_msg[11] = MAN_ID_L | 0x55;
    rdm_response_msg[12] = ((current_params.my_uid >> 24) & 0xFF) | 0xAA;
    rdm_response_msg[13] = ((current_params.my_uid >> 24) & 0xFF) | 0x55;
    rdm_response_msg[14] = ((current_params.my_uid >> 16) & 0xFF) | 0xAA;
    rdm_response_msg[15] = ((current_params.my_uid >> 16) & 0xFF) | 0x55;
    rdm_response_msg[16] = ((current_params.my_uid >> 8) & 0xFF) | 0xAA;
    rdm_response_msg[17] = ((current_params.my_uid >> 8) & 0xFF) | 0x55;
    rdm_response_msg[18] = ((current_params.my_uid >> 0) & 0xFF) | 0xAA;
    rdm_response_msg[19] = ((current_params.my_uid >> 0) & 0xFF) | 0x55;
    uint16_t crc = calc_crc(&rdm_response_msg[8], 12);
    rdm_response_msg[20] = ((crc >> 8) & 0xFF) | 0xAA;
    rdm_response_msg[21] = ((crc >> 8) & 0xFF) | 0x55;
    rdm_response_msg[22] = ((crc >> 0) & 0xFF) | 0xAA;
    rdm_response_msg[23] = ((crc >> 0) & 0xFF) | 0x55;

    TransmitDmxSlotValues(rdm_response_msg, 24, false);
}

///////////////////////////////////////////////////////////////////////////////
static void send_rdm_msg(uint8_t * rdm_msg, uint8_t command_class, uint8_t data_len)
{
    uint8_t count = 24 + data_len;

    delay_us(150);

    rdm_response_msg[0] = E120_SC_RDM;
    rdm_response_msg[1] = E120_SC_SUB_MESSAGE;
    rdm_response_msg[2] = count;
    rdm_response_msg[3] = rdm_msg[9];
    rdm_response_msg[4] = rdm_msg[10];
    rdm_response_msg[5] = rdm_msg[11];
    rdm_response_msg[6] = rdm_msg[12];
    rdm_response_msg[7] = rdm_msg[13];
    rdm_response_msg[8] = rdm_msg[14];
    rdm_response_msg[9] = MAN_ID_H;
    rdm_response_msg[10] = MAN_ID_L;
    rdm_response_msg[11] = (current_params.my_uid >> 24) & 0xFF;
    rdm_response_msg[12] = (current_params.my_uid >> 16) & 0xFF;
    rdm_response_msg[13] = (current_params.my_uid >> 8) & 0xFF;
    rdm_response_msg[14] = (current_params.my_uid >> 0) & 0xFF;
    rdm_response_msg[15] = rdm_msg[15];
    rdm_response_msg[16] = E120_RESPONSE_TYPE_ACK; // 0x00
    rdm_response_msg[17] = rdm_message_count;
    rdm_response_msg[18] = 0x00;
    rdm_response_msg[19] = 0x00;
    rdm_response_msg[20] = command_class;
    rdm_response_msg[21] = rdm_msg[21];
    rdm_response_msg[22] = rdm_msg[22];
    rdm_response_msg[23] = data_len;
    uint16_t crc = calc_crc(rdm_response_msg, count);
    rdm_response_msg[count + 0] = ((crc >> 8) & 0xFF);
    rdm_response_msg[count + 1] = ((crc >> 0) & 0xFF);

    TransmitDmxSlotValues(rdm_response_msg, (count + 2), true);
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_device_info_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = 0x01; // RDM Protocol Version HSB
    rdm_response_msg[25] = 0x00; // RDM Protocol Version LSB
    rdm_response_msg[26] = DEVICE_MODEL_ID1;
    rdm_response_msg[27] = DEVICE_MODEL_ID0;
    rdm_response_msg[28] = (E120_PRODUCT_CATEGORY_DIMMER_CS_LED >> 8) & 0xFF;
    rdm_response_msg[29] = (E120_PRODUCT_CATEGORY_DIMMER_CS_LED >> 0) & 0xFF;
    rdm_response_msg[30] = SW_VERSION_ID3;
    rdm_response_msg[31] = SW_VERSION_ID2;
    rdm_response_msg[32] = SW_VERSION_ID1;
    rdm_response_msg[33] = SW_VERSION_ID0;
    rdm_response_msg[34] = (NUMBER_OF_CHANNELS >> 8) & UINT8_MAX; // footprint size HSB
    rdm_response_msg[35] = NUMBER_OF_CHANNELS;                    // footprint size LSB
    rdm_response_msg[36] = 0x01;                                  // x: DMX512 Personality x(y);
    rdm_response_msg[37] = 0x01;                                  // y: DMX512 Personality x(y);
    rdm_response_msg[38] = (current_params.start_slot_num >> 8) & 0xFF;
    rdm_response_msg[39] = (current_params.start_slot_num >> 0) & 0xFF;
    rdm_response_msg[40] = 0x00; // Sub-Device Count HSB
    rdm_response_msg[41] = 0x00; // Sub-Device Count LSB
    rdm_response_msg[42] = 0x00; // Sensor Count

    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 19);
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_supported_parameters_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = (E120_DEVICE_MODEL_DESCRIPTION >> 8) & 0xFF;
    rdm_response_msg[25] = (E120_DEVICE_MODEL_DESCRIPTION >> 0) & 0xFF;
    rdm_response_msg[26] = (E120_MANUFACTURER_LABEL >> 8) & 0xFF;
    rdm_response_msg[27] = (E120_MANUFACTURER_LABEL >> 0) & 0xFF;
    rdm_response_msg[28] = (E120_DEVICE_LABEL >> 8) & 0xFF;
    rdm_response_msg[29] = (E120_DEVICE_LABEL >> 0) & 0xFF;
    rdm_response_msg[30] = (E120_DMX_START_ADDRESS >> 8) & 0xFF;
    rdm_response_msg[31] = (E120_DMX_START_ADDRESS >> 0) & 0xFF;

    // our custom feature specific PID
    rdm_response_msg[32] = (DE0_POWER_PERCENTAGE >> 8) & 0xFF;
    rdm_response_msg[33] = (DE0_POWER_PERCENTAGE >> 0) & 0xFF;
    rdm_response_msg[34] = (DE1_POWER_PERCENTAGE >> 8) & 0xFF;
    rdm_response_msg[35] = (DE1_POWER_PERCENTAGE >> 0) & 0xFF;
    rdm_response_msg[36] = (DE2_POWER_PERCENTAGE >> 8) & 0xFF;
    rdm_response_msg[37] = (DE2_POWER_PERCENTAGE >> 0) & 0xFF;

    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 14);
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_device_label_response(uint8_t * rdm_msg)
{
    dev_param_t stored;

    read_params(EEPROM_ADDRESS, &stored);

    for (uint8_t i = 0; i < 32; i++)
    {
        rdm_response_msg[24 + i] = stored.device_label[i];
    }

    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 32);
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_software_version_response(uint8_t * rdm_msg)
{
    char software_version_label[] = "VITRULUX";

    for (uint8_t i = 0; i < sizeof(software_version_label); i++)
    {
        rdm_response_msg[24 + i] = software_version_label[i];
    }

    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, sizeof(software_version_label));
}

///////////////////////////////////////////////////////////////////////////////
static void send_disc_mute_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = 0;
    rdm_response_msg[25] = 0;
    send_rdm_msg(rdm_msg, E120_DISCOVERY_COMMAND_RESPONSE, 2);
}

///////////////////////////////////////////////////////////////////////////////
static void send_disc_un_mute_response(uint8_t * rdm_msg) { send_disc_mute_response(rdm_msg); }

///////////////////////////////////////////////////////////////////////////////
static void send_get_dmx_start_address_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = (current_params.start_slot_num >> 8) & 0xFF;
    rdm_response_msg[25] = (current_params.start_slot_num >> 0) & 0xFF;
    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 2);
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_device_model_description_response(uint8_t * rdm_msg)
{
    char device_model_description[] = "1 channels @ 75mA";

    for (uint8_t i = 0; i < sizeof(device_model_description); i++)
    {
        rdm_response_msg[24 + i] = device_model_description[i];
    }

    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, sizeof(device_model_description));
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_manufacturer_label_response(uint8_t * rdm_msg)
{
    char manufacturer_label[] = "VITRULUX";

    for (uint8_t i = 0; i < sizeof(manufacturer_label); i++)
    {
        rdm_response_msg[24 + i] = manufacturer_label[i];
    }

    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, sizeof(manufacturer_label));
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_identify_device_response(uint8_t * rdm_msg, uint8_t state)
{
    rdm_response_msg[24] = state;
    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 1);
}

///////////////////////////////////////////////////////////////////////////////
static void send_set_dmx_start_address_response(uint8_t * rdm_msg)
{
    send_rdm_msg(rdm_msg, E120_SET_COMMAND_RESPONSE, 0);
}

///////////////////////////////////////////////////////////////////////////////
static void send_set_identify_device_response(uint8_t * rdm_msg)
{
    send_rdm_msg(rdm_msg, E120_SET_COMMAND_RESPONSE, 0);
}

///////////////////////////////////////////////////////////////////////////////
static void send_set_device_label_response(uint8_t * rdm_msg)
{
    send_rdm_msg(rdm_msg, E120_SET_COMMAND_RESPONSE, 0);
}

///////////////////////////////////////////////////////////////////////////////
static void set_device_label(uint8_t * rdm_msg)
{
    uint8_t symbol = 0;
    for (symbol = 0; symbol < rdm_msg[23]; symbol++)
    {
        current_params.device_label[symbol] = rdm_msg[24 + symbol];
    }
    for (symbol = rdm_msg[23]; symbol < 32; symbol++)
    {
        current_params.device_label[symbol] = ' ';
    }

    update_device_parameters();
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_parameter_description_response(uint8_t * rdm_msg)
{
    uint16_t PID;

    PID = rdm_msg[24];
    PID <<= 8;
    PID |= rdm_msg[25];

    switch (PID)
    {
        case DE0_POWER_PERCENTAGE:
        {
            rdm_response_msg[24] = rdm_msg[24];           // PID
            rdm_response_msg[25] = rdm_msg[25];           // PID
            rdm_response_msg[26] = 0x01;                  // PDL Size
            rdm_response_msg[27] = E120_DS_UNSIGNED_BYTE; // Data type
            rdm_response_msg[28] = E120_CC_GET_SET;       // Command Class
            rdm_response_msg[29] = 0x00;                  // Type. Must be 0
            rdm_response_msg[30] = E120_UNITS_NONE;       
            rdm_response_msg[31] = E120_PREFIX_NONE;      // Prefix
            // min valid value (32-bit)
            rdm_response_msg[32] = 0x00;                  
            rdm_response_msg[33] = 0x00;
            rdm_response_msg[34] = 0x00;
            rdm_response_msg[35] = 0x00;
            // max valid value (32-bit)
            rdm_response_msg[36] = 0x00;
            rdm_response_msg[37] = 0x00;
            rdm_response_msg[38] = 0x00;
            rdm_response_msg[39] = 0x64;
            // default value (32-bit)
            rdm_response_msg[40] = 0x00;
            rdm_response_msg[41] = 0x00;
            rdm_response_msg[42] = 0x00;
            rdm_response_msg[43] = 0x64;
            rdm_response_msg[44] = 'G';
            rdm_response_msg[45] = 'r';
            rdm_response_msg[46] = 'o';
            rdm_response_msg[47] = 'u';
            rdm_response_msg[48] = 'p';
            rdm_response_msg[49] = ' ';
            rdm_response_msg[50] = '1';
            rdm_response_msg[51] = ' ';
            rdm_response_msg[52] = 'p';
            rdm_response_msg[53] = 'o';
            rdm_response_msg[54] = 'w';
            rdm_response_msg[55] = 'e';
            rdm_response_msg[56] = 'r';
            rdm_response_msg[57] = ' ';
            rdm_response_msg[58] = 'l';
            rdm_response_msg[59] = 'e';
            rdm_response_msg[60] = 'v';
            rdm_response_msg[61] = 'e';
            rdm_response_msg[62] = 'l';
            rdm_response_msg[63] = ',';
            rdm_response_msg[64] = ' ';
            rdm_response_msg[65] = '%';
            rdm_response_msg[66] = 0x00;

            send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 43);
        }
        break;

        case DE1_POWER_PERCENTAGE:
        {
            rdm_response_msg[24] = rdm_msg[24];           // PID
            rdm_response_msg[25] = rdm_msg[25];           // PID
            rdm_response_msg[26] = 0x01;                  // PDL Size
            rdm_response_msg[27] = E120_DS_UNSIGNED_BYTE; // Data type
            rdm_response_msg[28] = E120_CC_GET_SET;       // Command Class
            rdm_response_msg[29] = 0x00;                  // Type. Must be 0
            rdm_response_msg[30] = E120_UNITS_NONE;
            rdm_response_msg[31] = E120_PREFIX_NONE;      // Prefix
            // min valid value (32-bit)
            rdm_response_msg[32] = 0x00;
            rdm_response_msg[33] = 0x00;
            rdm_response_msg[34] = 0x00;
            rdm_response_msg[35] = 0x00;
            // max valid value (32-bit)
            rdm_response_msg[36] = 0x00;
            rdm_response_msg[37] = 0x00;
            rdm_response_msg[38] = 0x00;
            rdm_response_msg[39] = 0x64;
            // default value (32-bit)
            rdm_response_msg[40] = 0x00;
            rdm_response_msg[41] = 0x00;
            rdm_response_msg[42] = 0x00;
            rdm_response_msg[43] = 0x64;
            rdm_response_msg[44] = 'G';
            rdm_response_msg[45] = 'r';
            rdm_response_msg[46] = 'o';
            rdm_response_msg[47] = 'u';
            rdm_response_msg[48] = 'p';
            rdm_response_msg[49] = ' ';
            rdm_response_msg[50] = '2';
            rdm_response_msg[51] = ' ';
            rdm_response_msg[52] = 'p';
            rdm_response_msg[53] = 'o';
            rdm_response_msg[54] = 'w';
            rdm_response_msg[55] = 'e';
            rdm_response_msg[56] = 'r';
            rdm_response_msg[57] = ' ';
            rdm_response_msg[58] = 'l';
            rdm_response_msg[59] = 'e';
            rdm_response_msg[60] = 'v';
            rdm_response_msg[61] = 'e';
            rdm_response_msg[62] = 'l';
            rdm_response_msg[63] = ',';
            rdm_response_msg[64] = ' ';
            rdm_response_msg[65] = '%';
            rdm_response_msg[66] = 0x00;

            send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 43);
        }
        break;

        case DE2_POWER_PERCENTAGE:
        {
            rdm_response_msg[24] = rdm_msg[24];           // PID
            rdm_response_msg[25] = rdm_msg[25];           // PID
            rdm_response_msg[26] = 0x01;                  // PDL Size
            rdm_response_msg[27] = E120_DS_UNSIGNED_BYTE; // Data type
            rdm_response_msg[28] = E120_CC_GET_SET;       // Command Class
            rdm_response_msg[29] = 0x00;                  // Type. Must be 0
            rdm_response_msg[30] = E120_UNITS_NONE;       
            rdm_response_msg[31] = E120_PREFIX_NONE;      // Prefix
            // min valid value (32-bit)
            rdm_response_msg[32] = 0x00;
            rdm_response_msg[33] = 0x00;
            rdm_response_msg[34] = 0x00;
            rdm_response_msg[35] = 0x00;
            // max valid value (32-bit)
            rdm_response_msg[36] = 0x00;
            rdm_response_msg[37] = 0x00;
            rdm_response_msg[38] = 0x00;
            rdm_response_msg[39] = 0x64;
            // default value (32-bit)
            rdm_response_msg[40] = 0x00;
            rdm_response_msg[41] = 0x00;
            rdm_response_msg[42] = 0x00;
            rdm_response_msg[43] = 0x64;
            rdm_response_msg[44] = 'G';
            rdm_response_msg[45] = 'r';
            rdm_response_msg[46] = 'o';
            rdm_response_msg[47] = 'u';
            rdm_response_msg[48] = 'p';
            rdm_response_msg[49] = ' ';
            rdm_response_msg[50] = '3';
            rdm_response_msg[51] = ' ';
            rdm_response_msg[52] = 'p';
            rdm_response_msg[53] = 'o';
            rdm_response_msg[54] = 'w';
            rdm_response_msg[55] = 'e';
            rdm_response_msg[56] = 'r';
            rdm_response_msg[57] = ' ';
            rdm_response_msg[58] = 'l';
            rdm_response_msg[59] = 'e';
            rdm_response_msg[60] = 'v';
            rdm_response_msg[61] = 'e';
            rdm_response_msg[62] = 'l';
            rdm_response_msg[63] = ',';
            rdm_response_msg[64] = ' ';
            rdm_response_msg[65] = '%';
            rdm_response_msg[66] = 0x00;

            send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 43);
        }
        break;

        default:
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_DE0_level_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = current_params.DE0_level;
    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 1);
}

///////////////////////////////////////////////////////////////////////////////
static void send_set_DE0_level_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = current_params.DE0_level;
    send_rdm_msg(rdm_msg, E120_SET_COMMAND_RESPONSE, 0);
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_DE1_level_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = current_params.DE1_level;
    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 1);
}

///////////////////////////////////////////////////////////////////////////////
static void send_set_DE1_level_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = current_params.DE1_level;
    send_rdm_msg(rdm_msg, E120_SET_COMMAND_RESPONSE, 0);
}

///////////////////////////////////////////////////////////////////////////////
static void send_get_DE2_level_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = current_params.DE2_level;
    send_rdm_msg(rdm_msg, E120_GET_COMMAND_RESPONSE, 1);
}

///////////////////////////////////////////////////////////////////////////////
static void send_set_DE2_level_response(uint8_t * rdm_msg)
{
    rdm_response_msg[24] = current_params.DE2_level;
    send_rdm_msg(rdm_msg, E120_SET_COMMAND_RESPONSE, 0);
}

///////////////////////////////////////////////////////////////////////////////
static void handle_rdm_msg(uint8_t * rdm_msg)
{
    if ((rdm_msg[1] == E120_SC_SUB_MESSAGE) && (rdm_msg[2] >= 24))
    {
        uint8_t length = rdm_msg[2];
        uint16_t crc = ((uint16_t)rdm_msg[length + 0] << 8) | rdm_msg[length + 1];
        if (calc_crc(rdm_msg, length) != crc)
            return;

        bool broadcast = check_broadcast(received_slots);
        bool own_uid = check_own_uid(received_slots);

        if (!broadcast && !own_uid)
            return; // it's not me

        uint16_t pid = (uint16_t)(rdm_msg[21] << 8) | rdm_msg[22]; // getting parameter ID

        switch (rdm_msg[20])
        {
            case E120_DISCOVERY_COMMAND:
            {
                if ((pid == E120_DISC_UNIQUE_BRANCH) && (length == 36))
                {
                    if (!rdm_muted)
                    {
                        long long lower_bound_uid;
                        long long upper_bound_uid;

                        lower_bound_uid = rdm_msg[24];
                        lower_bound_uid <<= 8;
                        lower_bound_uid |= rdm_msg[25];
                        lower_bound_uid <<= 8;
                        lower_bound_uid |= rdm_msg[26];
                        lower_bound_uid <<= 8;
                        lower_bound_uid |= rdm_msg[27];
                        lower_bound_uid <<= 8;
                        lower_bound_uid |= rdm_msg[28];
                        lower_bound_uid <<= 8;
                        lower_bound_uid |= rdm_msg[29];

                        upper_bound_uid = rdm_msg[30];
                        upper_bound_uid <<= 8;
                        upper_bound_uid |= rdm_msg[31];
                        upper_bound_uid <<= 8;
                        upper_bound_uid |= rdm_msg[32];
                        upper_bound_uid <<= 8;
                        upper_bound_uid |= rdm_msg[33];
                        upper_bound_uid <<= 8;
                        upper_bound_uid |= rdm_msg[34];
                        upper_bound_uid <<= 8;
                        upper_bound_uid |= rdm_msg[35];

                        if ((my_UID >= lower_bound_uid) && (my_UID <= upper_bound_uid))
                        {
                            send_disc_response(rdm_msg);
                        }
                    }
                    break;
                }

                else if ((pid == E120_DISC_MUTE) && (length == 24))
                {
                    if (own_uid == true)
                        send_disc_mute_response(rdm_msg);
                    rdm_muted = true;
                    break;
                }

                else if ((pid == E120_DISC_UN_MUTE) && (length == 24))
                {
                    rdm_muted = false;
                    if (own_uid == true)
                        send_disc_un_mute_response(rdm_msg);
                    break;
                }
            }
            break;


            case E120_GET_COMMAND:
                if (own_uid)
                {
                    if ((pid == E120_DEVICE_INFO) && (length == 24))
                    {
                        send_get_device_info_response(rdm_msg);
                    }
                    else if ((pid == E120_SOFTWARE_VERSION_LABEL) && (length == 24))
                    {
                        send_get_software_version_response(rdm_msg);
                    }
                    else if ((pid == E120_DMX_START_ADDRESS) && (length == 24))
                    {
                        send_get_dmx_start_address_response(rdm_msg);
                    }
                    else if ((pid == E120_IDENTIFY_DEVICE) && (length == 24))
                    {
                        send_get_identify_device_response(rdm_msg, identify_state);
                    }
                    else if ((pid == E120_SUPPORTED_PARAMETERS) && (length == 24))
                    {
                        send_get_supported_parameters_response(rdm_msg);
                    }
                    else if ((pid == E120_DEVICE_MODEL_DESCRIPTION) && (length == 24))
                    {
                        send_get_device_model_description_response(rdm_msg);
                    }
                    else if ((pid == E120_MANUFACTURER_LABEL) && (length == 24))
                    {
                        send_get_manufacturer_label_response(rdm_msg);
                    }
                    else if ((pid == E120_DEVICE_LABEL) && (length == 24))
                    {
                        send_get_device_label_response(rdm_msg);
                    }
                    else if ((pid == E120_PARAMETER_DESCRIPTION) && (length == 26))
                    {
                        send_get_parameter_description_response(rdm_msg);
                    }
                    else if ((pid == DE0_POWER_PERCENTAGE) && (length == 24))
                    {
                        send_get_DE0_level_response(rdm_msg);
                    }
                    else if ((pid == DE1_POWER_PERCENTAGE) && (length == 24))
                    {
                        send_get_DE1_level_response(rdm_msg);
                    }
                    else if ((pid == DE2_POWER_PERCENTAGE) && (length == 24))
                    {
                        send_get_DE2_level_response(rdm_msg);
                    }
                }
                break;


            case E120_SET_COMMAND:
                if ((pid == E120_DMX_START_ADDRESS) && (length == 26))
                {
                    current_params.start_slot_num = (rdm_msg[24] << 8) | rdm_msg[25];
                    if (own_uid)
                        send_set_dmx_start_address_response(rdm_msg);
                    update_device_parameters();
                }
                else if ((pid == E120_IDENTIFY_DEVICE) && (length == 25))
                {
                    identify_state = rdm_msg[24];
                    intensity = (identify_state) ? UINT8_MAX : NULL;
                    uint8_t init_dim_val[NUMBER_OF_CHANNELS];
                    memset(init_dim_val, intensity, sizeof(init_dim_val));
                    dim_leds(init_dim_val);

                    if (own_uid)
                    {
                        send_set_identify_device_response(rdm_msg);
                    }
                }
                else if ((pid == E120_DEVICE_LABEL) && (rdm_msg[23] <= 32))
                {
                    if (own_uid)
                    {
                        send_set_device_label_response(rdm_msg);
                        set_device_label(rdm_msg);
                    }
                }
                else if ((pid == DE0_POWER_PERCENTAGE) && (length == 25))
                {
                    current_params.DE0_level = rdm_msg[24];
                    XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE0, power_percentage[current_params.DE0_level]);
                    update_device_parameters();
                    XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE0);
                    send_set_DE0_level_response(rdm_msg);
                }
                else if ((pid == DE1_POWER_PERCENTAGE) && (length == 25))
                {
                    current_params.DE1_level = rdm_msg[24];
                    XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE1, power_percentage[current_params.DE1_level]);
                    update_device_parameters();
                    XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE1);
                    send_set_DE1_level_response(rdm_msg);
                }
                else if ((pid == DE0_POWER_PERCENTAGE) && (length == 25))
                {
                    current_params.DE0_level = rdm_msg[24];
                    XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE2, power_percentage[current_params.DE2_level]);
                    update_device_parameters();
                    XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE2);
                    send_set_DE2_level_response(rdm_msg);
                }
                break;

            default:
                break;
        }
    }
}