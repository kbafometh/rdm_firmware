/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*            (c) 2014 - 2018 SEGGER Microcontroller GmbH             *
*                                                                    *
*           www.segger.com     Support: support@segger.com           *
*                                                                    *
**********************************************************************
*                                                                    *
* All rights reserved.                                               *
*                                                                    *
* Redistribution and use in source and binary forms, with or         *
* without modification, are permitted provided that the following    *
* conditions are met:                                                *
*                                                                    *
* - Redistributions of source code must retain the above copyright   *
*   notice, this list of conditions and the following disclaimer.    *
*                                                                    *
* - Neither the name of SEGGER Microcontroller GmbH                  *
*   nor the names of its contributors may be used to endorse or      *
*   promote products derived from this software without specific     *
*   prior written permission.                                        *
*                                                                    *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND             *
* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,        *
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF           *
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
* DISCLAIMED.                                                        *
* IN NO EVENT SHALL SEGGER Microcontroller GmbH BE LIABLE FOR        *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  *
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;    *
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF      *
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          *
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  *
* USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH   *
* DAMAGE.                                                            *
*                                                                    *
**********************************************************************

-------------------------- END-OF-HEADER -----------------------------

File    : main.c
Purpose : Generic application start

*/

#include <stdio.h>
#include <stdlib.h>
#include <xmc1200.h>

#include "dev_logger.h"
#include "peripheral_config.h"
#include "RDM_functions.h"


#define TICKS_PER_SECOND	100000	// ��� ������ 10��� - ������� ��� �������� � ����, ��� ���������� ������� ����� �������� �������� ���
uint32_t tick = 0;

XMC_SCU_CLOCK_CONFIG_t clock_config = 
{
    .pclk_src = XMC_SCU_CLOCK_PCLKSRC_DOUBLE_MCLK,
    .rtc_src = XMC_SCU_CLOCK_RTCCLKSRC_DCO2,
    .fdiv = 0,
    .idiv = 1,
};

/*********************************************************************
 *
 *       main()
 *
 *  Function description
 *   Application entry point.
 */
void main(void)
{

    SystemInit();
    LoggerInit();

    XMC_SCU_CLOCK_Init(&clock_config);
    SysTick_Config(SystemCoreClock/TICKS_PER_SECOND);

    NC_pin_init();
    bccu_init();
    uart_init();
    ccu4_init();
    prng_init();
	
#ifdef PRODUCTION
    disable_debug();	// ��������� SWD, ��� ��� �� �������� 8 � 9 ����� BCCU. 
			// �������� ����� ������� �� �����, ������� ��������� ��� ���������
#endif
	
    XMC_SCU_StartTempMeasurement();
	
    rdm_init();
	
    work_mode();	//�������� � ����������� �����
}

void USIC0_0_IRQHandler(void) 
{   // ���������� ���������� �� �����. ������ ����� �������������� ����� ������, ������� ������ � ��
    handle_dmx_msg();
}

void CCU40_0_IRQHandler(void) 
{   // ����������� ���������� ��������.
    XMC_CCU4_SLICE_ClearEvent(CCU40_CC40, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);  
    XMC_CCU4_EnableShadowTransfer(CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_0);
}

void CCU40_1_IRQHandler(void) 
{
    XMC_CCU4_SLICE_ClearEvent(CCU40_CC41, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
    XMC_CCU4_EnableShadowTransfer(CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_1);
}

void CCU40_2_IRQHandler(void) 
{
    XMC_CCU4_SLICE_ClearEvent(CCU40_CC42, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
    XMC_CCU4_EnableShadowTransfer(CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_2);
}

void CCU40_3_IRQHandler(void) 
{
    XMC_CCU4_SLICE_ClearEvent(CCU40_CC43, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
    XMC_CCU4_EnableShadowTransfer(CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_3);
}

void SysTick_Handler(void) 
{   //SysTick ������������ ��� ����������� ��������. ��� ���� - 10���
    tick++;
}

/*************************** End of file ****************************/