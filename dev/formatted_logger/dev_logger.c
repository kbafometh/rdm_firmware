#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>

#include "dev_logger.h"

static bool isTimestampEnabled = false;
bool IsTimestampEnabled(void) { return isTimestampEnabled; }
//-------------------------------------------------------------------
void EnableTimestamp(void)
{
#if LOG_TIMESTAMP_ENABLED
    isTimestampEnabled = true;
#endif
}
//-------------------------------------------------------------------
#if LOG_TIMESTAMP_ENABLED
char * GetCurTime(void)
{
    static char g_curTime[] = "000.00.00.000";

    uint64_t timeMs = 0;//dev_hw_clock(); - call RTC or SysTick

    uint32_t curTimeInt[4] = {0};
    memset((void*)g_curTime, 0x30, sizeof(g_curTime)); // fill by '0'
    g_curTime[3] = 0x2E; g_curTime[6] = 0x2E; g_curTime[9] = 0x2E; g_curTime[13] = 0; // 0x2E -> fill by '.', 0 - null terminated

    curTimeInt[0] =   timeMs / 3600000; 			// hours
    curTimeInt[1] =  (timeMs % 3600000) / 60000; 		// minutes
    curTimeInt[2] = ((timeMs % 3600000) % 60000) / 1000; 	// seconds
    curTimeInt[3] = ((timeMs % 3600000) % 60000) % 1000; 	// msec
    int j = 13;
    for (int i = 3; i >= 0; --i )
    {
        while ( curTimeInt[i] != 0 )
        {
            g_curTime[--j] = curTimeInt[i] % 10 + '0';
            curTimeInt[i] = curTimeInt[i] / 10;
        }
        while ((g_curTime[--j] != '.') && j > 0 );
    }
    return g_curTime;
}
#endif //LOG_TIMESTAMP_ENABLED

//---------------------------------
void LoggerInit(void)
{
    SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 101, SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);

    LOG_CLEAR();
}
