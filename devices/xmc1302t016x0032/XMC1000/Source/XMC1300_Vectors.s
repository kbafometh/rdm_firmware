/*********************************************************************
*                    SEGGER Microcontroller GmbH                     *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*            (c) 2014 - 2018 SEGGER Microcontroller GmbH             *
*                                                                    *
*           www.segger.com     Support: support@segger.com           *
*                                                                    *
**********************************************************************
*                                                                    *
* All rights reserved.                                               *
*                                                                    *
* Redistribution and use in source and binary forms, with or         *
* without modification, are permitted provided that the following    *
* conditions are met:                                                *
*                                                                    *
* - Redistributions of source code must retain the above copyright   *
*   notice, this list of conditions and the following disclaimer.    *
*                                                                    *
* - Neither the name of SEGGER Microcontroller GmbH                  *
*   nor the names of its contributors may be used to endorse or      *
*   promote products derived from this software without specific     *
*   prior written permission.                                        *
*                                                                    *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND             *
* CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,        *
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF           *
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE           *
* DISCLAIMED.                                                        *
* IN NO EVENT SHALL SEGGER Microcontroller GmbH BE LIABLE FOR        *
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR           *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT  *
* OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;    *
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF      *
* LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT          *
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  *
* USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH   *
* DAMAGE.                                                            *
*                                                                    *
*********************************************************************/

/*****************************************************************************
 *                         Preprocessor Definitions                          *
 *                         ------------------------                          *
 * VECTORS_IN_RAM                                                            *
 *                                                                           *
 *   If defined, an area of RAM will large enough to store the vector table  *
 *   will be reserved.                                                       *
 *                                                                           *
 *****************************************************************************/

  .syntax unified
  .code 16

  .section .init, "ax"
  .align 2

/*****************************************************************************
 * Default Exception Handlers                                                *
 *****************************************************************************/

  .thumb_func
  .weak NMI_Handler
NMI_Handler:
  b .

  .thumb_func
  .weak HardFault_Handler
HardFault_Handler:
  b .

  .thumb_func
  .weak SVC_Handler
SVC_Handler:
  b .

  .thumb_func
  .weak PendSV_Handler
PendSV_Handler:
  b .

  .thumb_func
  .weak SysTick_Handler
SysTick_Handler:
  b .

  .thumb_func
Dummy_Handler:
  b .

#if defined(__OPTIMIZATION_SMALL)

  .weak SCU_0_IRQHandler
  .thumb_set SCU_0_IRQHandler,Dummy_Handler

  .weak SCU_1_IRQHandler
  .thumb_set SCU_1_IRQHandler,Dummy_Handler

  .weak SCU_2_IRQHandler
  .thumb_set SCU_2_IRQHandler,Dummy_Handler

  .weak ERU0_0_IRQHandler
  .thumb_set ERU0_0_IRQHandler,Dummy_Handler

  .weak ERU0_1_IRQHandler
  .thumb_set ERU0_1_IRQHandler,Dummy_Handler

  .weak ERU0_2_IRQHandler
  .thumb_set ERU0_2_IRQHandler,Dummy_Handler

  .weak ERU0_3_IRQHandler
  .thumb_set ERU0_3_IRQHandler,Dummy_Handler

  .weak MATH0_0_IRQHandler
  .thumb_set MATH0_0_IRQHandler,Dummy_Handler

  .weak USIC0_0_IRQHandler
  .thumb_set USIC0_0_IRQHandler,Dummy_Handler

  .weak USIC0_1_IRQHandler
  .thumb_set USIC0_1_IRQHandler,Dummy_Handler

  .weak USIC0_2_IRQHandler
  .thumb_set USIC0_2_IRQHandler,Dummy_Handler

  .weak USIC0_3_IRQHandler
  .thumb_set USIC0_3_IRQHandler,Dummy_Handler

  .weak USIC0_4_IRQHandler
  .thumb_set USIC0_4_IRQHandler,Dummy_Handler

  .weak USIC0_5_IRQHandler
  .thumb_set USIC0_5_IRQHandler,Dummy_Handler

  .weak VADC0_C0_0_IRQHandler
  .thumb_set VADC0_C0_0_IRQHandler,Dummy_Handler

  .weak VADC0_C0_1_IRQHandler
  .thumb_set VADC0_C0_1_IRQHandler,Dummy_Handler

  .weak VADC0_G0_0_IRQHandler
  .thumb_set VADC0_G0_0_IRQHandler,Dummy_Handler

  .weak VADC0_G0_1_IRQHandler
  .thumb_set VADC0_G0_1_IRQHandler,Dummy_Handler

  .weak VADC0_G1_0_IRQHandler
  .thumb_set VADC0_G1_0_IRQHandler,Dummy_Handler

  .weak VADC0_G1_1_IRQHandler
  .thumb_set VADC0_G1_1_IRQHandler,Dummy_Handler

  .weak CCU40_0_IRQHandler
  .thumb_set CCU40_0_IRQHandler,Dummy_Handler

  .weak CCU40_1_IRQHandler
  .thumb_set CCU40_1_IRQHandler,Dummy_Handler

  .weak CCU40_2_IRQHandler
  .thumb_set CCU40_2_IRQHandler,Dummy_Handler

  .weak CCU40_3_IRQHandler
  .thumb_set CCU40_3_IRQHandler,Dummy_Handler

  .weak CCU80_0_IRQHandler
  .thumb_set CCU80_0_IRQHandler,Dummy_Handler

  .weak CCU80_1_IRQHandler
  .thumb_set CCU80_1_IRQHandler,Dummy_Handler

  .weak POSIF0_0_IRQHandler
  .thumb_set POSIF0_0_IRQHandler,Dummy_Handler

  .weak POSIF0_1_IRQHandler
  .thumb_set POSIF0_1_IRQHandler,Dummy_Handler

  .weak BCCU0_0_IRQHandler
  .thumb_set BCCU0_0_IRQHandler,Dummy_Handler

#else

  .thumb_func
  .weak SCU_0_IRQHandler
SCU_0_IRQHandler:
  b .

  .thumb_func
  .weak SCU_1_IRQHandler
SCU_1_IRQHandler:
  b .

  .thumb_func
  .weak SCU_2_IRQHandler
SCU_2_IRQHandler:
  b .

  .thumb_func
  .weak ERU0_0_IRQHandler
ERU0_0_IRQHandler:
  b .

  .thumb_func
  .weak ERU0_1_IRQHandler
ERU0_1_IRQHandler:
  b .

  .thumb_func
  .weak ERU0_2_IRQHandler
ERU0_2_IRQHandler:
  b .

  .thumb_func
  .weak ERU0_3_IRQHandler
ERU0_3_IRQHandler:
  b .

  .thumb_func
  .weak MATH0_0_IRQHandler
MATH0_0_IRQHandler:
  b .

  .thumb_func
  .weak USIC0_0_IRQHandler
USIC0_0_IRQHandler:
  b .

  .thumb_func
  .weak USIC0_1_IRQHandler
USIC0_1_IRQHandler:
  b .

  .thumb_func
  .weak USIC0_2_IRQHandler
USIC0_2_IRQHandler:
  b .

  .thumb_func
  .weak USIC0_3_IRQHandler
USIC0_3_IRQHandler:
  b .

  .thumb_func
  .weak USIC0_4_IRQHandler
USIC0_4_IRQHandler:
  b .

  .thumb_func
  .weak USIC0_5_IRQHandler
USIC0_5_IRQHandler:
  b .

  .thumb_func
  .weak VADC0_C0_0_IRQHandler
VADC0_C0_0_IRQHandler:
  b .

  .thumb_func
  .weak VADC0_C0_1_IRQHandler
VADC0_C0_1_IRQHandler:
  b .

  .thumb_func
  .weak VADC0_G0_0_IRQHandler
VADC0_G0_0_IRQHandler:
  b .

  .thumb_func
  .weak VADC0_G0_1_IRQHandler
VADC0_G0_1_IRQHandler:
  b .

  .thumb_func
  .weak VADC0_G1_0_IRQHandler
VADC0_G1_0_IRQHandler:
  b .

  .thumb_func
  .weak VADC0_G1_1_IRQHandler
VADC0_G1_1_IRQHandler:
  b .

  .thumb_func
  .weak CCU40_0_IRQHandler
CCU40_0_IRQHandler:
  b .

  .thumb_func
  .weak CCU40_1_IRQHandler
CCU40_1_IRQHandler:
  b .

  .thumb_func
  .weak CCU40_2_IRQHandler
CCU40_2_IRQHandler:
  b .

  .thumb_func
  .weak CCU40_3_IRQHandler
CCU40_3_IRQHandler:
  b .

  .thumb_func
  .weak CCU80_0_IRQHandler
CCU80_0_IRQHandler:
  b .

  .thumb_func
  .weak CCU80_1_IRQHandler
CCU80_1_IRQHandler:
  b .

  .thumb_func
  .weak POSIF0_0_IRQHandler
POSIF0_0_IRQHandler:
  b .

  .thumb_func
  .weak POSIF0_1_IRQHandler
POSIF0_1_IRQHandler:
  b .

  .thumb_func
  .weak BCCU0_0_IRQHandler
BCCU0_0_IRQHandler:
  b .

#endif

/*****************************************************************************
 * Vector Table                                                              *
 *****************************************************************************/

  .section .vectors, "ax"
  .align 2
  .global _vectors
  .extern __stack_end__
  .extern Reset_Handler

_vectors:
  .word __stack_end__
  .word Reset_Handler
  .word NMI_Handler
  .word HardFault_Handler
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word SVC_Handler
  .word 0 /* Reserved */
  .word 0 /* Reserved */
  .word PendSV_Handler
  .word SysTick_Handler
  .word SCU_0_IRQHandler
  .word SCU_1_IRQHandler
  .word SCU_2_IRQHandler
  .word ERU0_0_IRQHandler
  .word ERU0_1_IRQHandler
  .word ERU0_2_IRQHandler
  .word ERU0_3_IRQHandler
  .word MATH0_0_IRQHandler
  .word Dummy_Handler /* Reserved */
  .word USIC0_0_IRQHandler
  .word USIC0_1_IRQHandler
  .word USIC0_2_IRQHandler
  .word USIC0_3_IRQHandler
  .word USIC0_4_IRQHandler
  .word USIC0_5_IRQHandler
  .word VADC0_C0_0_IRQHandler
  .word VADC0_C0_1_IRQHandler
  .word VADC0_G0_0_IRQHandler
  .word VADC0_G0_1_IRQHandler
  .word VADC0_G1_0_IRQHandler
  .word VADC0_G1_1_IRQHandler
  .word CCU40_0_IRQHandler
  .word CCU40_1_IRQHandler
  .word CCU40_2_IRQHandler
  .word CCU40_3_IRQHandler
  .word CCU80_0_IRQHandler
  .word CCU80_1_IRQHandler
  .word POSIF0_0_IRQHandler
  .word POSIF0_1_IRQHandler
  .word Dummy_Handler /* Reserved */
  .word Dummy_Handler /* Reserved */
  .word BCCU0_0_IRQHandler
_vectors_end:

#ifdef VECTORS_IN_RAM
  .section .vectors_ram, "ax"
  .align 2
  .global _vectors_ram

_vectors_ram:
  .space _vectors_end - _vectors, 0
#endif
