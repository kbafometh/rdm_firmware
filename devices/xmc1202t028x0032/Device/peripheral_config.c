#include "peripheral_config.h"
#include "RDM_functions.h"


uint32_t fade_prescaler = 0x03;
uint16_t DE0_level = 100;
uint16_t DE1_level = 100;
uint16_t DE2_level = 100;
const uint8_t ccu_channels = NUMBER_OF_CHANNELS - 9;


const uint16_t NO_OF_BCCU_CHANNELS[9] = {0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F, 0x7F, 0xFF, 0x1FF};



const XMC_GPIO_CONFIG_t NC_pin_config = {
    .mode = XMC_GPIO_MODE_INPUT_PULL_DOWN, .input_hysteresis = XMC_GPIO_INPUT_HYSTERESIS_STANDARD};

void NC_pin_init(void)
{
    XMC_GPIO_Init(P0_0, &NC_pin_config);
    XMC_GPIO_Init(P0_12, &NC_pin_config);
    XMC_GPIO_Init(P2_0, &NC_pin_config);
    XMC_GPIO_Init(P2_1, &NC_pin_config);
    XMC_GPIO_Init(P2_2, &NC_pin_config);
    XMC_GPIO_Init(P2_5, &NC_pin_config);
    XMC_GPIO_Init(P2_6, &NC_pin_config);
    XMC_GPIO_Init(P2_7, &NC_pin_config);
    XMC_GPIO_Init(P2_8, &NC_pin_config);
    XMC_GPIO_SetMode(BACKDOOR_PIN, XMC_GPIO_MODE_INPUT_PULL_UP); //BMI UPM
}

//  BCCU
const XMC_GPIO_CONFIG_t PWM_config = {
#ifdef BCR430
    .mode = XMC_GPIO_MODE_OUTPUT_OPEN_DRAIN_ALT1,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_HIGH
#elif BCR402
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT1,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW
#endif
};

const XMC_GPIO_CONFIG_t unused_pin_config = {
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL, .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW};

XMC_BCCU_GLOBAL_CONFIG_t bccu_global_config = {
    .fclk_ps = 0x10,                    // 4MHz @PCLK 64MHz (divider 16)
    .dclk_ps = 0x6D,                    // 300kHz
    .bclk_sel = XMC_BCCU_BCLK_MODE_FAST // 4MHz bit stream
};

XMC_BCCU_DIM_CONFIG_t bccu_dim_config = {
    .dim_div = 0x0, .dither_en = 1, .cur_sel = XMC_BCCU_DIM_CURVE_FINE};

XMC_BCCU_CH_CONFIG_t pdm_global_config = {.pack_thresh = 3,
    .pack_en = true,
    .dim_sel = XMC_BCCU_CH_DIMMING_SOURCE_DE0,
    .flick_wd_en = XMC_BCCU_CH_FLICKER_WD_EN};

XMC_GPIO_CONFIG_t PWM_CCU40_config = {
    .mode = XMC_GPIO_MODE_OUTPUT_OPEN_DRAIN_ALT2, .output_level = XMC_GPIO_OUTPUT_LEVEL_HIGH};

void bccu_init(void)
{
    XMC_GPIO_CONFIG_t config;
    for (int i = 0; i < 9; i++)
    {
        config = (i < NUMBER_OF_CHANNELS) ? PWM_config : unused_pin_config;
        switch (i)
        {
            case 0:
                XMC_GPIO_Init(CH1R_PIN, &config);
                break;
            case 1:
                XMC_GPIO_Init(CH1G_PIN, &config);
                break;
            case 2:
                XMC_GPIO_Init(CH1B_PIN, &config);
                break;
            case 3:
                XMC_GPIO_Init(CH2R_PIN, &config);
                break;
            case 4:
                XMC_GPIO_Init(CH2G_PIN, &config);
                break;
            case 5:
                XMC_GPIO_Init(CH2B_PIN, &config);
                break;
            case 6:
                XMC_GPIO_Init(CH3R_PIN, &config);
                break;
            case 7:
                XMC_GPIO_Init(CH3G_PIN, &config);
                break;
            case 8:
                XMC_GPIO_Init(CH3B_PIN, &config);
                break;
        }
    }
    XMC_BCCU_GlobalInit(BCCU0, &bccu_global_config);
    XMC_BCCU_ConcurrentSetOutputPassiveLevel(BCCU0, NO_OF_BCCU_CHANNELS[NUM_OF_BCCU_CHANNELS - 1],
        XMC_BCCU_CH_ACTIVE_LEVEL_LOW); // only after XMC_BCCU_GlobalInit function!
    for (int i = 0; i < 9; i++)
    {
        switch (i)
        {
            case 0:
                XMC_BCCU_CH_Init(CH1R_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH1R_PWM, fade_prescaler);
                break;
            case 1:
                XMC_BCCU_CH_Init(CH1G_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH1G_PWM, fade_prescaler);
                break;
            case 2:
                XMC_BCCU_CH_Init(CH1B_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH1B_PWM, fade_prescaler);
                break;
            case 3:
                XMC_BCCU_CH_Init(CH2R_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH2R_PWM, fade_prescaler);
                break;
            case 4:
                XMC_BCCU_CH_Init(CH2G_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH2G_PWM, fade_prescaler);
                break;
            case 5:
                XMC_BCCU_CH_Init(CH2B_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH2B_PWM, fade_prescaler);
                break;
            case 6:
                XMC_BCCU_CH_Init(CH3R_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH3R_PWM, fade_prescaler);
                break;
            case 7:
                XMC_BCCU_CH_Init(CH3G_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH3G_PWM, fade_prescaler);
                break;
            case 8:
                XMC_BCCU_CH_Init(CH3B_PWM, &pdm_global_config);
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH3B_PWM, fade_prescaler);
                break;
        }
    }
    XMC_BCCU_DIM_Init(BCCU0_DE0, &bccu_dim_config);
    XMC_BCCU_ConcurrentEnableChannels(BCCU0, NO_OF_BCCU_CHANNELS[NUM_OF_BCCU_CHANNELS - 1]);
    XMC_BCCU_EnableDimmingEngine(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE0);
    XMC_BCCU_DIM_SetTargetDimmingLevel(BCCU0_DE0, 4095U);
    XMC_BCCU_StartDimming(BCCU0, XMC_BCCU_CH_DIMMING_SOURCE_DE0);

    uint8_t init_dim_val[NUMBER_OF_CHANNELS] = {0};
    dim_leds(init_dim_val);
}

void dim_leds(uint8_t * channels)
{
    for (int i = 0; i < NUMBER_OF_CHANNELS; i++)
    {
        uint16_t channel_value = 0;
        channel_value = ((channels[i] << 4) + (channels[i] >> 4));

        switch (i)
        {
            case 0:
                XMC_BCCU_CH_SetTargetIntensity(CH1R_PWM, channel_value);
                break;
            case 1:
                XMC_BCCU_CH_SetTargetIntensity(CH1G_PWM, channel_value);
                break;
            case 2:
                XMC_BCCU_CH_SetTargetIntensity(CH1B_PWM, channel_value);
                break;
            case 3:
                XMC_CCU4_SLICE_SetTimerCompareMatch(CCU40_CC41, channel_value);
                XMC_CCU4_EnableShadowTransfer(CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_1);
                break;
            case 4:
                XMC_BCCU_CH_SetTargetIntensity(CH2R_PWM, channel_value);
                break;
            case 5:
                XMC_BCCU_CH_SetTargetIntensity(CH2G_PWM, channel_value);
                break;
            case 6:
                XMC_BCCU_CH_SetTargetIntensity(CH2B_PWM, channel_value);
                break;
            case 7:
                XMC_CCU4_SLICE_SetTimerCompareMatch(CCU40_CC42, channel_value);
                XMC_CCU4_EnableShadowTransfer(CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_2);
                break;
            case 8:
                XMC_BCCU_CH_SetTargetIntensity(CH3R_PWM, channel_value);
                break;
            case 9:
                XMC_BCCU_CH_SetTargetIntensity(CH3G_PWM, channel_value);
                break;
            case 10:
                XMC_BCCU_CH_SetTargetIntensity(CH3B_PWM, channel_value);
                break;
            case 11:
                XMC_CCU4_SLICE_SetTimerCompareMatch(CCU40_CC43, channel_value);
                XMC_CCU4_EnableShadowTransfer(CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_3);
                break;
        }
    }
    XMC_BCCU_ConcurrentStartLinearWalk(BCCU0, NO_OF_BCCU_CHANNELS[NUM_OF_BCCU_CHANNELS - 1]);
}

void tune_fade_time(void)
{
    uint32_t linear_walk_complete = 0;
    for (int i = 0; i < NUMBER_OF_CHANNELS; i++)
        linear_walk_complete |= XMC_BCCU_IsLinearWalkComplete(BCCU0, i);
    if (linear_walk_complete)
    {
        fade_prescaler--;
        XMC_BCCU_ConcurrentAbortLinearWalk(BCCU0, NO_OF_BCCU_CHANNELS[NUM_OF_BCCU_CHANNELS - 1]);
    }
    else
    {
        fade_prescaler++;
    }
    for (int i = 0; i < NUMBER_OF_CHANNELS; i++)
    {
        switch (i)
        {
            case 0:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH1R_PWM, fade_prescaler);
                break;
            case 1:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH1G_PWM, fade_prescaler);
                break;
            case 2:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH1B_PWM, fade_prescaler);
                break;
            case 3:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH2R_PWM, fade_prescaler);
                break;
            case 4:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH2G_PWM, fade_prescaler);
                break;
            case 5:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH2B_PWM, fade_prescaler);
                break;
            case 6:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH3R_PWM, fade_prescaler);
                break;
            case 7:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH3G_PWM, fade_prescaler);
                break;
            case 8:
                XMC_BCCU_CH_SetLinearWalkPrescaler(CH3B_PWM, fade_prescaler);
                break;
        }
    }
}

// UART
XMC_USIC_CH_t *DMX = XMC_UART0_CH1;

const XMC_UART_CH_CONFIG_t dmx_config = 
{ 
    .baudrate = 250000U,
    .data_bits = 8U,
    .frame_length = 8U,
    .oversampling = 16U,
    .stop_bits = 2,
    .parity_mode = XMC_USIC_CH_PARITY_MODE_NONE
};

const XMC_GPIO_CONFIG_t DMX_dir_config = 
{
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL, 
    .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW
};

const XMC_GPIO_CONFIG_t DMX_rx_config = 
{
    .mode = XMC_GPIO_MODE_INPUT_TRISTATE, 
    .input_hysteresis = XMC_GPIO_INPUT_HYSTERESIS_STANDARD
};

const XMC_GPIO_CONFIG_t DMX_tx_config = 
{
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT7
};

void uart_init(void)
{
    XMC_GPIO_Init(DIR_PIN, &DMX_dir_config);
    XMC_GPIO_Init(TX_PIN, &DMX_tx_config);
    XMC_GPIO_Init(RX_PIN, &DMX_rx_config);

    XMC_GPIO_EnableDigitalInput(DIR_PIN);
    XMC_GPIO_EnableDigitalInput(TX_PIN);
    XMC_GPIO_EnableDigitalInput(RX_PIN);

    XMC_UART_CH_Init(DMX, &dmx_config);
    XMC_UART_CH_SetInputSource(DMX, XMC_UART_CH_INPUT_RXD, USIC0_C1_DX0_DX3INS);
    XMC_UART_CH_SetInputSource(DMX, XMC_UART_CH_INPUT_RXD1, USIC0_C1_DX3_P2_9);

    XMC_USIC_CH_RXFIFO_Configure(DMX, 0U, XMC_USIC_CH_FIFO_SIZE_2WORDS, 0U);

    XMC_USIC_CH_RXFIFO_SetInterruptNodePointer(
        DMX, XMC_USIC_CH_RXFIFO_INTERRUPT_NODE_POINTER_STANDARD, 0);

    XMC_UART_CH_EnableEvent(DMX, (uint32_t)XMC_UART_CH_EVENT_SYNCHRONIZATION_BREAK);
    XMC_USIC_CH_SetInterruptNodePointer(DMX, XMC_USIC_CH_INTERRUPT_NODE_POINTER_PROTOCOL, 0);
    XMC_USIC_CH_SetInterruptNodePointer(DMX, XMC_USIC_CH_INTERRUPT_NODE_POINTER_TRANSMIT_BUFFER, 0);
    NVIC_SetPriority(USIC0_0_IRQn, 3U);
    NVIC_EnableIRQ(USIC0_0_IRQn);

    XMC_UART_CH_Start(DMX);
}

XMC_CCU4_SLICE_COMPARE_CONFIG_t PWM_SLICE_config = {
    .timer_mode = XMC_CCU4_SLICE_TIMER_COUNT_MODE_EA,
    .monoshot = XMC_CCU4_SLICE_TIMER_REPEAT_MODE_REPEAT,
    .shadow_xfer_clear = 0,
    .dither_timer_period = 0,
    .dither_duty_cycle = 0,
    .prescaler_mode = XMC_CCU4_SLICE_PRESCALER_MODE_NORMAL,
    .mcm_enable = 0,
    .prescaler_initval = XMC_CCU4_SLICE_PRESCALER_16, // For fPWM = 15.625kHz
    .float_limit = 0,
    .dither_limit = 0,
    .passive_level = XMC_CCU4_SLICE_OUTPUT_PASSIVE_LEVEL_LOW, 
    .timer_concatenation = 0};


XMC_CCU4_SLICE_EVENT_CONFIG_t CCU_PWM_event_config = {
    .mapped_input = XMC_CCU4_SLICE_INPUT_I, /* mapped to SCU.GSC40 */
    .edge = XMC_CCU4_SLICE_EVENT_EDGE_SENSITIVITY_RISING_EDGE,
    .level = XMC_CCU4_SLICE_EVENT_LEVEL_SENSITIVITY_ACTIVE_HIGH,
    .duration = XMC_CCU4_SLICE_EVENT_FILTER_3_CYCLES};

void ccu4_init(void)
{
    XMC_CCU4_Init(CCU40, XMC_CCU4_SLICE_MCMS_ACTION_TRANSFER_PR_CR);
    XMC_CCU4_StartPrescaler(CCU40);
    XMC_CCU4_SetModuleClock(CCU40, XMC_CCU4_CLOCK_SCU);

    XMC_CCU4_SLICE_CompareInit(CCU40_CC40, &PWM_SLICE_config);
    XMC_CCU4_SLICE_CompareInit(CCU40_CC41, &PWM_SLICE_config);
    XMC_CCU4_SLICE_CompareInit(CCU40_CC42, &PWM_SLICE_config);
    XMC_CCU4_SLICE_CompareInit(CCU40_CC43, &PWM_SLICE_config);

    XMC_CCU4_SLICE_SetTimerCompareMatch(CCU40_CC40, NULL);
    XMC_CCU4_SLICE_SetTimerCompareMatch(CCU40_CC41, NULL);
    XMC_CCU4_SLICE_SetTimerCompareMatch(CCU40_CC42, NULL);
    XMC_CCU4_SLICE_SetTimerCompareMatch(CCU40_CC43, NULL);

    XMC_CCU4_SLICE_SetTimerPeriodMatch(CCU40_CC41, CCU4_PERIOD);
    XMC_CCU4_SLICE_SetTimerPeriodMatch(CCU40_CC41, CCU4_PERIOD);
    XMC_CCU4_SLICE_SetTimerPeriodMatch(CCU40_CC42, CCU4_PERIOD);
    XMC_CCU4_SLICE_SetTimerPeriodMatch(CCU40_CC43, CCU4_PERIOD);

    XMC_CCU4_EnableShadowTransfer(
        CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_0 | XMC_CCU4_SHADOW_TRANSFER_PRESCALER_SLICE_0);
    XMC_CCU4_EnableShadowTransfer(
        CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_1 | XMC_CCU4_SHADOW_TRANSFER_PRESCALER_SLICE_1);
    XMC_CCU4_EnableShadowTransfer(
        CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_2 | XMC_CCU4_SHADOW_TRANSFER_PRESCALER_SLICE_2);
    XMC_CCU4_EnableShadowTransfer(
        CCU40, XMC_CCU4_SHADOW_TRANSFER_SLICE_3 | XMC_CCU4_SHADOW_TRANSFER_PRESCALER_SLICE_3);

    XMC_CCU4_SLICE_ConfigureEvent(CCU40_CC40, XMC_CCU4_SLICE_EVENT_0, &CCU_PWM_event_config);
    XMC_CCU4_SLICE_ConfigureEvent(CCU40_CC41, XMC_CCU4_SLICE_EVENT_0, &CCU_PWM_event_config);
    XMC_CCU4_SLICE_ConfigureEvent(CCU40_CC42, XMC_CCU4_SLICE_EVENT_0, &CCU_PWM_event_config);
    XMC_CCU4_SLICE_ConfigureEvent(CCU40_CC43, XMC_CCU4_SLICE_EVENT_0, &CCU_PWM_event_config);

    XMC_CCU4_SLICE_EnableEvent(CCU40_CC40, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
    XMC_CCU4_SLICE_EnableEvent(CCU40_CC41, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
    XMC_CCU4_SLICE_EnableEvent(CCU40_CC42, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);
    XMC_CCU4_SLICE_EnableEvent(CCU40_CC43, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP);

    XMC_CCU4_SLICE_StartConfig(
        CCU40_CC40, XMC_CCU4_SLICE_EVENT_0, XMC_CCU4_SLICE_START_MODE_TIMER_START_CLEAR);
    XMC_CCU4_SLICE_StartConfig(
        CCU40_CC41, XMC_CCU4_SLICE_EVENT_0, XMC_CCU4_SLICE_START_MODE_TIMER_START_CLEAR);
    XMC_CCU4_SLICE_StartConfig(
        CCU40_CC42, XMC_CCU4_SLICE_EVENT_0, XMC_CCU4_SLICE_START_MODE_TIMER_START_CLEAR);
    XMC_CCU4_SLICE_StartConfig(
        CCU40_CC43, XMC_CCU4_SLICE_EVENT_0, XMC_CCU4_SLICE_START_MODE_TIMER_START_CLEAR);

    XMC_CCU4_SLICE_SetInterruptNode(
        CCU40_CC40, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP, XMC_CCU4_SLICE_SR_ID_0);
    XMC_CCU4_SLICE_SetInterruptNode(
        CCU40_CC41, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP, XMC_CCU4_SLICE_SR_ID_1);
    XMC_CCU4_SLICE_SetInterruptNode(
        CCU40_CC42, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP, XMC_CCU4_SLICE_SR_ID_2);
    XMC_CCU4_SLICE_SetInterruptNode(
        CCU40_CC43, XMC_CCU4_SLICE_IRQ_ID_COMPARE_MATCH_UP, XMC_CCU4_SLICE_SR_ID_3);

    NVIC_SetPriority(CCU40_0_IRQn, 2U);
    NVIC_SetPriority(CCU40_1_IRQn, 2U);
    NVIC_SetPriority(CCU40_2_IRQn, 2U);
    NVIC_SetPriority(CCU40_3_IRQn, 2U);

    NVIC_EnableIRQ(CCU40_0_IRQn);
    NVIC_EnableIRQ(CCU40_1_IRQn);
    NVIC_EnableIRQ(CCU40_2_IRQn);
    NVIC_EnableIRQ(CCU40_3_IRQn);

    XMC_CCU4_EnableMultipleClocks(CCU40, 0xF);

    XMC_CCU4_SLICE_StartTimer(CCU40_CC40);
    XMC_CCU4_SLICE_StartTimer(CCU40_CC41);
    XMC_CCU4_SLICE_StartTimer(CCU40_CC42);
    XMC_CCU4_SLICE_StartTimer(CCU40_CC43);

    XMC_GPIO_Init(CH1W_PIN, &PWM_CCU40_config);
    XMC_GPIO_Init(CH2W_PIN, &PWM_CCU40_config);
    XMC_GPIO_Init(CH3W_PIN, &PWM_CCU40_config);
}


void disable_debug(void)
{
    uint32_t bmi_value;

    bmi_value = XMC_FLASH_ReadWord((uint32_t *)0x10000E00);
    if ((bmi_value & 0x0000ffffU) != 0xF8C1U)
    {
        XMC1000_BmiInstallationReq(0xF8C1);
    }
    if (!XMC_GPIO_GetInput(BACKDOOR_PIN))
    {
        XMC1000_BmiInstallationReq(0xF8C3);
    }
}


void prng_init(void)
{
    uint32_t * UCIDptr;
    uint32_t UniqueChipID[4] = {0, 0, 0, 0};
    uint32_t Count;
    UCIDptr = (uint32_t *)0x10000FF0;
    uint16_t UCID[8];
    for (Count = 0; Count < 4; Count++)
    {
        UniqueChipID[Count] = *UCIDptr;
        UCID[2 * Count] = (UniqueChipID[Count] >> 16) & 0xFFFF;
        UCID[2 * Count + 1] = (UniqueChipID[Count] >> 0) & 0xFFFF;
        UCIDptr++;
    }
    XMC_PRNG_INIT_t prng_config = {.key_words[0] = UCID[0],
        .key_words[1] = UCID[1],
        .key_words[2] = UCID[2],
        .key_words[3] = UCID[3],
        .key_words[4] = UCID[4],
        .key_words[5] = UCID[5],
        .key_words[6] = UCID[6],
        .key_words[7] = UCID[7],
        .block_size = XMC_PRNG_RDBS_BYTE};

    XMC_PRNG_Init(&prng_config);
}


void read_params(uint32_t * address, dev_param_t * parameters)
{
    uint8_t parameters_size = (sizeof(dev_param_t) / XMC_FLASH_BYTES_PER_BLOCK) + 1;
    XMC_FLASH_ReadBlocks(address, (uint32_t *)parameters, parameters_size);
    while (XMC_FLASH_GetStatus() != XMC_FLASH_STATUS_OK)
    {
    }
}

void write_params(uint32_t * address, dev_param_t * parameters)
{
    uint8_t parameters_size = (sizeof(dev_param_t) / XMC_FLASH_BYTES_PER_BLOCK) + 1;

    __disable_irq();
    XMC_FLASH_ErasePage(address);
    while (XMC_FLASH_GetStatus() != XMC_FLASH_STATUS_OK)
    {
    }
    XMC_FLASH_WriteBlocks(address, (uint32_t *)parameters, parameters_size, true);
    while (XMC_FLASH_GetStatus() != XMC_FLASH_STATUS_OK)
    {
    }
    __enable_irq();
}

// EOF